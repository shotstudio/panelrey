<footer>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 logo">
				<figure><img src="assets/images/panel-rey-logotipo.png" alt="Panel Rey"></figure>
			</div>
			<div class="hidden-xs hidden-sm col-md-12 col-lg-9 menu-footer">
				<ul>
					<li><a href="nosotros.php">Nosotros</a></li>
					<li><a href="galeria.php">Galería</a></li>
					<li><a href="servicios.php">Servicios</a></li>
					<li><a href="videos.php">Videos</a></li>
					<li><a href="beneficios.php">Beneficios</a></li>
					<li><a href="contacto.php">Contacto</a></li>
					<li><a href="aviso-privacidad.php">Aviso de Privacidad</a></li>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-3"></div>

			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-3 contacto-footer">
				<p>Envíanos un correo a</p>
				<a href="mailto:residencialpr@gpromax.com">residencialpr@gpromax.com</a>
				<p>¡Llámanos!</p>
				<a href="tel:(81)8183406455">01 800 PANEL REY</a>
			</div>

			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-2 redes">
				<p>¡Síguenos!</p>
				<a href="https://www.facebook.com/PanelReyMX" target="in_blank"><i class="fa fa-facebook-square"></i></a>
				<a href="http://www.twitter.com/panelreymexico" target="in_blank"><i class="fa fa-twitter-square"></i></a>
				<a href="https://www.youtube.com/channel/UCUd0_vG7LuP7eGACqn6bDGQ" target="in_blank"><i class="fa fa-youtube-square"></i></a>
			</div>

			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 suscribete">
				<p>Suscríbete a nuestro boletín mensual</p>
				<!-- <form class="form-inline">
					<div class="form-group">
						<input type="email" class="form-control" id="exampleInputEmail3">
				  	</div>
				  <button type="button" class="btn btn-naranja">ENVIAR</button>
				</form> -->
					<form class="form-inline" method="post" action="https://app.icontact.com/icp/signup.php" name="icpsignup" id="icpsignup1291" accept-charset="UTF-8" onsubmit="return verifyRequired1291();" >
					<input type="hidden" name="redirect" value="http://http://shotmty.com/panel-rey/">
					<input type="hidden" name="errorredirect" value="http://http://shotmty.com/panel-rey/">

				<div id="SignUp">
				<table width="100%" class="signupframe" border="0" cellspacing="0" cellpadding="5">
					<tr>
				      <td>
				        <input class="form-boletin" type="text" name="fields_email">
				      </td>
				    </tr>
					    <input type="hidden" name="listid" value="28544">
					    <input type="hidden" name="specialid:28544" value="XZUJ">
					    <input type="hidden" name="clientid" value="1446747">
					    <input type="hidden" name="formid" value="1291">
					    <input type="hidden" name="reallistid" value="1">
					    <input type="hidden" name="doubleopt" value="0">
				    <!-- <tr>
				      <td> </td>
				      <td><span class="required">*</span> = Required Field</td>
				    </tr> -->
				    <tr>
				    	<td><input class="btn btn-boletin" type="submit" name="Submit" value="Enviar"></td>
				    </tr>
				    </table>
				</div>
				</form>


			</div> <!-- cierra div col -->

		</div>
	</div>
</footer>

<script src="assets/js/min/panel-rey-min.js"></script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-76871544-1', 'auto');
  ga('send', 'pageview');

</script>

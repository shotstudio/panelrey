
<?php include('top.php'); ?>
	<title>Panel Rey| Servicios</title>
</head>
<body>
<?php include('sidebar.php'); ?>
<div class="supercont">

	<?php include('header.php'); ?>

	<div class="banner banner-servicios">
		<div class="caption">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-6 col-md-offtet-* texto-banner">
						<h1>Servicios</h1>
					</div>
				</div>
			</div>
		</div>
		<!-- <figure>
			<img class="hidden-xs" src="images/banner-servicios-panel-rey.jpg" height="375" width="1400">
			<img class="visible-xs" src="images/banner-servicios-panel-rey-xs.jpg" height="375" width="530">
		</figure> -->
	</div>

	<div class="servicios">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12 texto-intro">
					<h2>Construye el Hogar de tus sueños, en Panel Rey te asesoramos y te ofrecemos los siguientes servicios.</h2>
				</div>
			</div>

			<div class="row seccion1">
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<img class="wow zoomIn" src="assets/images/i-asesoria.svg" height="42" width="61" alt="Asesoría Técnica">
					<h3>Asesoría Técnica</h3>
					<p>Te resolvemos todas las dudas que tengas en el desarrollo de tu proyecto, brindándote una solución profesional, sólida y respaldada por la experiencia de nuestro equipo de asesores especializados y certificados.
					</p>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<img class="wow zoomIn" src="assets/images/i-especificacion.svg" height="42" width="42" alt="Especificación">
					<h3>Especificación</h3>
					<p>Te indicamos los materiales mas eficientes para cada necesidad de tu proyecto ya que contamos con productos de la mejor calidad y versatilidad.
					</p>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<img class="wow zoomIn" src="assets/images/i-calculo.svg" height="42" width="32" alt="Cálculo Estructural">
					<h3>Cálculo Estructural</h3>
					<p>Te ayudamos a calcular estructuralmente tu proyecto de construcción de una manera eficiente y respaldada por el conocimiento y la experiencia de una empresa sólida y de primera calidad.
					</p>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<img class="wow zoomIn" src="assets/images/i-contacto.svg" height="42" width="55" alt="Contacto con Constructores">
					<h3>Contacto con Constructores</h3>
					<p>En Panel Rey te contactamos con personal calificado para realizar tu proyecto de construcción de una manera profesional y eficiente.
					</p>
				</div>
			</div>

			<div class="row seccion2">
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<img class="wow zoomIn" src="assets/images/i-cuantificacion.svg" height="42" width="64" alt="Cuantificación">
					<h3>Cuantificación</h3>
					<p>Te apoyamos indicándote las cantidades de material  de Panel Rey necesarias para tu proyecto.
					</p>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<img class="wow zoomIn" src="assets/images/i-desarollo.svg" height="42" width="45" alt="Desarrollo de Planos y Detalles Constructivos">
					<h3>Desarrollo de Planos y Detalles Constructivos</h3>
					<p>Tu nos compartes el proyecto Arquitectónico y nosotros desarrollamos los planos técnicos de nuestro sistema hecho a la medida de tu diseño.
					</p>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<img class="wow zoomIn" src="assets/images/i-supervision.png" height="31" width="67" alt="Supervisión">
					<h3>Supervisión</h3>
					<p>No estas solo, te visitamos en la construcción de tu proyecto para asesorarte en caso de requerirlo.</p>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<img class="wow zoomIn" src="assets/images/i-capacitacion.svg" height="42" width="50" alt="Capacitación">
					<h3>Capacitación</h3>
					<p>Quieres aprender como construir con nuestro sistema o tienes personal para construir? Nosotros te ofrecemos cursos teóricos y prácticos SIN COSTO.
					</p>
				</div>
			</div>

		</div>
	</div>
	


		
	

	<?php include('footer.php'); ?>
</div> <!-- cierra super content -->

<?php include('bottom.php'); ?>

<?php include('top.php'); ?>
	<title>Panel Rey | Residencia Tec</title>
</head>
<body>
<?php include('sidebar.php'); ?>
<div class="supercont">

	<?php include('header.php'); ?>

<div class="linea"></div>
<div class="galeria-interior">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 intro-interior">
				<h1>Residencia Tec</h1>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<ol class="breadcrumb">
				  <li><a href="galeria.php">Galería</a></li>
				  <li>Residencia Tec</li>
				</ol>
			</div>
		</div>
	</div>
</div>

<div class="galeria-casa">
	<div class="gallery js-flickity" data-flickity-options='{ "freeScroll": true, "wrapAround": true }'>
		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-Tec-2013/1.jpg" height="480" width="640">
		</div>

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-Tec-2013/2.jpg" height="480" width="640">
		</div>

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-Tec-2013/3.jpg" height="480" width="640">
		</div> 

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-Tec-2013/4.jpg" height="480" width="640">
		</div> 

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-Tec-2013/5.jpg" height="480" width="640">
		</div> 

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-Tec-2013/6.jpg" height="480" width="640">
		</div> 

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-Tec-2013/7.jpg" height="480" width="640">
		</div> 

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-Tec-2013/8.jpg" height="480" width="640">
		</div> 

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-Tec-2013/9.jpg" height="480" width="640">
		</div> 

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-Tec-2013/10.jpg" height="480" width="640">
		</div> 

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-Tec-2013/11.jpg" height="480" width="640">
		</div> 

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-Tec-2013/12.jpg" height="480" width="640">
		</div> 
	</div>
</div>

<div class="info-casa">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
				<ul>
					<li>Residencial del sorteo TEC de Monterrey construida con material Panel Rey en sus plafones corridos.</li>
					<li>Arquitecto Ernesto Vela.</li>
				</ul>
			</div>
		</div>
	</div>
</div>

<iframe src="https://www.google.com/maps/embed?pb=!1m0!3m2!1sen!2smx!4v1456765302891!6m8!1m7!1sCiVE4CXOCCu0ORnTbs6rFQ!2m2!1d25.650996!2d-100.3446426!3f47.569870871523925!4f-0.5915033756720334!5f0.7820865974627469" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>	

	<?php include('footer.php'); ?>
</div> <!-- cierra super content -->
<script src="assets/js/flickity.pkgd.min.js"></script>

<?php include('bottom.php'); ?>
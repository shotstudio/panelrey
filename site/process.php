<?php
// sleep(1);
header('Content-Type: application/json');

//Array de la informacion
$datos = trim( $_POST['datos'] );
parse_str( $datos, $da );

// CAPTCHA
if( isset($da['g-recaptcha-response']) && strlen($da['g-recaptcha-response']) > 0 )
{
	$captcha = $da['g-recaptcha-response'];
} else {
	echo json_encode(array('status'=>500, 'msj'=>'Verifica que no eres un robot.'));
	exit;
}

// VERIFICAR CAPTCHA
$response = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LeKIh0TAAAAACA7qemNW83ssH-mk7-4ZuBuI_qH&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
if( $response['success'] == false )
{
	echo json_encode(array('status'=>500, 'msj'=>'Response is false'));
	exit;
} else {

	// phpmailer---------------------------------------------------------------------
	require_once('assets/php/class.phpmailer.php');
	// clases
	require_once 'assets/php/class.php';

	$limp = new Limpiar();
	$nombre = $limp->String( $da['nombre'] );
	$email = $limp->String( $da['email'] );
	$telefono = $limp->String( $da['telefono'] );
	$estado = $limp->String( $da['estado'] );
	$mensaje = $limp->String( $da['mensaje'] );

	$mail = new PHPMailer;
	$mail->Subject = 'Forma de contacto';
	//$mail->addAddress('juancarlos.shot@gmail.com', 'Juan Carlos');
	//$mail->addAddress('juancarlos.shot@gmail.com', 'Juan Carlos');
	$mail->addAddress('residencialpr@gpromax.com', 'Panel Rey Residencial');
	$mail->From = $email;
	$mail->FromName = $nombre;

	// prepare email body text
	$Body = "";
	$Body .= "<strong>Nombre:</strong> ";
	$Body .= $nombre;
	$Body .= "<br><br>";

	$Body .= "<strong>Email:</strong> ";
	$Body .= $email;
	$Body .= "<br><br>";

	$Body .= "<strong>Teléfono:</strong> ";
	$Body .= $telefono;
	$Body .= "<br><br>";

	$Body .= "<strong>Estado:</strong> ";
	$Body .= $estado;
	$Body .= "<br><br>";

	$Body .= "<strong>Mensaje:</strong> ";
	$Body .= $mensaje;


	$mail->isHTML(true); // Set email format to HTML

	$mail->CharSet = 'UTF-8';
	$mail->Body    = $Body;

	if(!$mail->send()) {
		echo json_encode(array('status'=>500, 'msj'=>$mail->ErrorInfo));
	} else {
		echo json_encode(array('status'=>200, 'msj'=>'Mensaje enviado correctamente'));
	}
	//phpmailer---------------------------------------------------------------------

}

?>

<?php include('top.php'); ?>
	<title>Panel Rey | Áticos en Puerta de Hierro</title>
</head>
<body>
<?php include('sidebar.php'); ?>
<div class="supercont">

	<?php include('header.php'); ?>

<div class="linea"></div>
<div class="galeria-interior">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 intro-interior">
				<h1>Áticos en Puerta de Hierro Monterrey</h1>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<ol class="breadcrumb">
				  <li><a href="galeria.php">Galería</a></li>
				  <li>Áticos en Puerta de Hierro Monterrey</li>
				</ol>
			</div>
		</div>
	</div>
</div>

<div class="galeria-casa">
	<div class="gallery js-flickity" data-flickity-options='{ "freeScroll": true, "wrapAround": true }'>
		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/aticos/1.jpg" height="480" width="640">

		</div>
		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/aticos/2.jpg" height="480" width="640">

		</div>
		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/aticos/3.jpg" height="480" width="640">

		</div>
		 <div class="gallery-cell">
			<img src="assets/images/galeria-thumb/aticos/4.jpg" height="480" width="640">
		</div> 
	</div>
</div>

<div class="info-casa">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<ul>
					<li>El sistema constructivo Panel Rey es una nueva forma de construir un piso extra en una residencia o casa habitación (áticos).</li>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<ul>
					<li>Con un 15% extra en la inversión total, se obtiene hasta un 50% más de espacio habitable, y se tienen ahorros en energía eléctrica de hasta 30%.</li>
				</ul>
			</div>
		</div>
	</div>
</div>

<iframe src="https://www.google.com/maps/embed?pb=!1m0!3m2!1sen!2smx!4v1456245534734!6m8!1m7!1sDbzCu_5jCqmBd2xNmN2ZwA!2m2!1d25.75739736776457!2d-100.4150785802969!3f339.03867917873373!4f12.200857609786766!5f0.4000000000000002" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
<!-- <div id="street-view"></div> -->
		
	

	<?php include('footer.php'); ?>
</div> <!-- cierra super content -->

<script src="assets/js/flickity.pkgd.min.js"></script>

<!-- 
 <script>
	var panorama;
	function initialize() {
	  panorama = new google.maps.StreetViewPanorama(
	      document.getElementById('street-view'),
	      {
	        position: {lat: 25.652711, lng: -100.395644},
	        pov: {heading: 165, pitch: 0},
	        zoom: 1
	      });
	}
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCNTteA4iVLLqy0rfY-2dInBCfawFFMFHc&signed_in=true&callback=initialize"></script> -->

<?php include('bottom.php'); ?>







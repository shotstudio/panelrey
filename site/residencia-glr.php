
<?php include('top.php'); ?>
	<title>Panel Rey | Residencia GLR</title>
</head>
<body>
<?php include('sidebar.php'); ?>
<div class="supercont">

	<?php include('header.php'); ?>

<div class="linea"></div>
<div class="galeria-interior">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 intro-interior">
				<h1>Residencia GLR</h1>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<ol class="breadcrumb">
				  <li><a href="galeria.php">Galería</a></li>
				  <li>Residencia GLR</li>
				</ol>
			</div>
		</div>
	</div>
</div>

<div class="galeria-casa">
	<div class="gallery js-flickity" data-flickity-options='{ "freeScroll": true, "wrapAround": true }'>
		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-GLR/1.jpg" height="480" width="640">
		</div>

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-GLR/3.jpg" height="480" width="640">
		</div> 

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-GLR/4.jpg" height="480" width="640">
		</div>

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-GLR/5.jpg" height="480" width="640">
		</div>

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-GLR/6.jpg" height="480" width="640">
		</div>

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-GLR/7.jpg" height="480" width="640">
		</div>

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-GLR/8.jpg" height="480" width="640">
		</div>

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-GLR/9.jpg" height="480" width="640">
		</div>


		

	</div>
</div>

<div class="info-casa">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
				<ul>
					<li>Hermosa residencia construida en su totalidad con el sistema constructivo Panel Rey, sus interiores, muros divisorios, plafones corridos y fachadas.</li>
					<li>Realizada por el Arquitecto Gilberto L. Rodríguez.</li>
				</ul>
			</div>
			<!-- <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<ul>
					<li>Inicia la operación de la planta Panel Rey</li>
					<li>En El Carmen, N. L., con canteras propias de donde extrae la roca de yeso de la más alta pureza.</li>
					<li>Lorem Ipsum</li>
				</ul>
			</div> -->
		</div>
	</div>
</div>
<iframe src="https://www.google.com/maps/embed?pb=!1m0!3m2!1sen!2smx!4v1456246248174!6m8!1m7!1sMPgNFvFYpKd6gTirTeolxQ!2m2!1d25.6522962!2d-100.3953789!3f89.53327164452571!4f0!5f0.7820865974627469" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

	<?php include('footer.php'); ?>
</div> <!-- cierra super content -->
<script src="assets/js/flickity.pkgd.min.js"></script>

 
<?php include('bottom.php'); ?>
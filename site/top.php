<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="HandheldFriendly" content="True">
	<meta name="description" content="El sistema más innovador para la construcción de viviendas, con asesoría personalizada. ¡Conoce ya la innovación residencial!">
	<meta name="keywords" content="construcción, panel, sistema constructivo, remodelación, residencial, viviendas, asesoría, remodelación, ampliaciones," />

	<link rel="shortcut icon" type="assets/image/x-icon" href="assets/images/favicon.ico">
	<link rel="apple-touch-icon" href="assets/images/phone.jpg">
	<link rel="apple-touch-icon" sizes="72x72" href="assets/images/tablet.jpg">
	<link rel="apple-touch-icon" sizes="114x114" href="assets/images/retina.jpg">
	<link rel="stylesheet" href="assets/css/styles.min.css">

	<!--[if lt IE 9]>
	<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

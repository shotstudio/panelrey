
<?php include('top.php'); ?>
	<title>Panel Rey | Residencia Tec</title>
</head>
<body>
<?php include('sidebar.php'); ?>
<div class="supercont">

	<?php include('header.php'); ?>

<div class="linea"></div>
<div class="galeria-interior">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 intro-interior">
				<h1>Residencia Tec</h1>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<ol class="breadcrumb">
				  <li><a href="galeria.php">Galería</a></li>
				  <li>Residencia Tec</li>
				</ol>
			</div>
		</div>
	</div>
</div>

<div class="galeria-casa">
	<div class="gallery js-flickity" data-flickity-options='{ "freeScroll": true, "wrapAround": true }'>
		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-Tec-2010/1.jpg" height="480" width="640">
		</div>

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-Tec-2010/2.jpg" height="480" width="640">
		</div>

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-Tec-2010/3.jpg" height="480" width="640">
		</div> 

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-Tec-2010/4.jpg" height="480" width="640">
		</div> 

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-Tec-2010/5.jpg" height="480" width="640">
		</div> 

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-Tec-2010/6.jpg" height="480" width="640">
		</div> 

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-Tec-2010/7.jpg" height="480" width="640">
		</div> 

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-Tec-2010/8.jpg" height="480" width="640">
		</div> 

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-Tec-2010/9.jpg" height="480" width="640">
		</div> 

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-Tec-2010/10.jpg" height="480" width="640">
		</div> 

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-Tec-2010/11.jpg" height="480" width="640">
		</div> 
		
	</div>
</div>

<div class="info-casa">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
				<ul>
					<li>Residencial del sorteo TEC de Monterrey construida con material Panel Rey en sus plafones corridos.</li>
					<li>Arquitecto Pablo Ferrara.</li>
				</ul>
			</div>
		</div>
	</div>
</div>

<iframe src="https://www.google.com/maps/embed?pb=!1m0!3m2!1sen!2smx!4v1456764991501!6m8!1m7!1stNxVvtFsyt3EY0UchezxDg!2m2!1d25.65254934188833!2d-100.3416344435454!3f317.51186369902837!4f-8.15424102927659!5f0.7820865974627469" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
	

	<?php include('footer.php'); ?>
</div> <!-- cierra super content -->
<script src="assets/js/flickity.pkgd.min.js"></script>



<?php include('bottom.php'); ?>
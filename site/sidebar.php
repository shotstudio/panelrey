<div class="sidebar escondido">
	<div class="sidenav">
		<a class="side-close"><i class="fa fa-times"></i></a>
		<nav>
			<ul>
				<li class="cont">
					<a href="nosotros.php">Nosotros</a>
				</li>
				<li class="cont">
					<a href="sistema-constructivo.php">Sistema Constructivo</a>
				</li>
				<li class="cont">
					<a href="galeria.php">Galería</a>
				</li>
				<li class="cont">
					<a href="residencia-panel-rey.php">Residencia</a>
				</li>
				<li class="cont">
					<a href="casa-panel-rey.php">Casa</a>
				</li>
				<li class="cont">
					<a href="servicios.php">Servicios</a>
				</li>
				<li class="cont">
					<a href="videos.php">Videos</a>
				</li>
				<li class="cont">
					<a href="beneficios.php">Beneficios</a>
				</li>
				<li class="cont">
					<a href="leed.php">LEED</a>
				</li>
				<li class="cont">
					<a href="contacto.php">Contacto</a>
				</li>
				<li class="social">
					<a href="https://www.facebook.com/PanelReyMX" target="in_blank"><i class="fa fa-facebook-square"></i></a>
					<a href="http://www.twitter.com/panelreymexico" target="in_blank"><i class="fa fa-twitter-square"></i></a>
					<a href="https://www.youtube.com/channel/UCUd0_vG7LuP7eGACqn6bDGQ" target="in_blank"><i class="fa fa-youtube-square"></i></a>
				</li>
				<li class="contacto">
					<a href="tel:(81)83450055"><i class="fa fa-phone"></i></a>
					<a href="mailto:residencialpr@gpromax.com"><i class="fa fa-envelope"></i></a>
				</li>
			</ul>
		</nav>
	</div>
</div>

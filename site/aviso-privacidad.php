
<?php include('top.php'); ?>
	<title>Contacto | Panel Rey</title>
</head>
<body>
<?php include('sidebar.php'); ?>
<div class="supercont">

	<?php include('header.php'); ?>
	<div class="linea"></div>

	<div class="t-aviso">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6">
					<h1>Aviso de Privacidad</h1>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 logo-promax">
					<figure> <img src="assets/images/Grupo-Promax-Logo.png" alt="Grupo Promax" height="160" width="394"> </figure>
				</div>
			</div>

		</div>
	</div>

	<div class="aviso-p">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<p>En términos de lo previsto en la <strong>Ley Federal de Protección de Datos Personales en Posesión de los Particulares</strong> le pedimos leer los Términos y Condiciones del presente <strong>Aviso de Privacidad,</strong> la aportación que se haga de datos personales, aún los de carácter sensible por medio de los diferentes portales u oficinas de servicio de las empresas de <strong>Grupo Promax,</strong> constituye la aceptación expresa de los presentes Términos y Condiciones, autorizando a su uso y transmisión entre las empresas filiales y/o subsidiarias de <strong>Grupo Promax:</strong></p>
					<p>1. El responsable de la salvaguarda de la información proporcionada será <strong>Grupo Promax,</strong> con domicilio en Serafín Peña 938 Sur, Centro en Monterrey, N.L. México, CP: 64000. El presente Aviso tiene por objeto la protección de los datos personales, a efecto de garantizar su privacidad, manejo y el derecho del aportante de dicha información a su eliminación y/o modificación efectiva.</p>
					<p>2. Al hacer entrega de información personal por cualquier medio, ya sea electrónico o físico, el otorgante acepta y autoriza a Grupo Promax a utilizar y tratar de forma automatizada sus datos personales e información suministrados, los cuales pasarán a formar parte de la base de datos de cualquiera de las empresas filiales y/o subsidiarias de <strong>Grupo Promax.</strong></p>
					<p>3. Algunos de los datos que se recabarán serán los siguientes :</p>

					<ul>
						<li>Nombre completo</li>
						<li>Edad</li>
						<li>Fecha de nacimiento</li>
						<li>Género</li>
						<li>Estado Civil</li>
						<li>Domicilio</li>
						<li>Nacionalidad</li>
						<li>Correo electrónico</li>
						<li>Teléfono particular, del trabajo, celular</li>
						<li>Forma de contacto preferida</li>
						<li>Información sobre cómo se enteró de nuestros productos y servicios</li>
					</ul>
					<p>4.  El uso que se le dará a la información será el siguiente :</p>
					<ul>
						<li>Desarrollo de nuevos productos y servicios</li>
						<li>Conocer el mercado y el desempeño de la empresa dentro de éste</li>
						<li>Seguimiento a las ventas a fin de alcanzar la satisfacción del cliente</li>
						<li>Estudios para determinar la satisfacción del consumidor</li>
						<li>Conformación de expedientes de consumidores</li>
						<li>Creación de directorios</li>
						<li>Interrelación de bases de datos para conocer el perfil y las necesidades del consumidor</li>
						<li>Envío de promociones sobre nuestros productos o servicios</li>
						<li>Entrega de promociones</li>
						<li>Entrega de premios de lealtad</li>
						<li>Comunicaciones de todo tipo</li>
						<li>Invitaciones a eventos</li>
						<li>Realizar análisis a fin de determinar la eficacia de publicidad</li>
						<li>Para integrar bases de datos de candidatos para suplir vacantes</li>
						<li>Para integrar bases de datos del personal contratado</li></ul>

						<p>5. La Ley Federal de Protección de Datos Personales en Posesión de los Particulares establece los derechos del otorgante para oponerse al manejo de la información que éste haya proporcionado. La solicitud podrá realizarse por escrito libre dirigido al correo electrónico privacidad@gpromax.com o físicamente a Grupo Promax, RH Protección de Datos, Serafín Peña 938 Sur, Centro en Monterrey, N.L. México, CP: 64000
						<br/>
						La solicitud deberá contener los siguientes datos :</p>

					<ul>
						<li>Nombre y domicilio u otro medio para comunicar la respuesta a la solicitud;</li>
						<li>Copia de los documentos que acrediten la identidad del otorgante o, en su caso, la representación legal de su representante:</li>
						<li>Identificación clara y precisa de los datos personales respecto de los que se busca ejercer alguno de los derechos de acceso, rectificación, cancelación y oposición; y</li>
						<li>Cualquier otro elemento que facilite la localización de los datos requeridos.</li>
						<li>La rectificación de datos personales, deberá de acompañarse de los documentos probatorios idóneos para sustentar los cambios. La solicitud se responderá en un plazo de quince días hábiles a partir de la fecha de recepción.</li>
					</ul>

					<p>6. <strong>Grupo Promax</strong> se reserva el derecho de modificar y/o cambiar el presente Aviso de Privacidad en cualquier momento.</p>
					<p><strong>Julio 2011.</strong></p>

				</div>
			</div>

		</div>
	</div>



	<?php include('footer.php'); ?>
</div> <!-- cierra super content -->


<?php include('bottom.php'); ?>

<?php include('top.php'); ?>
	<title>Residencia | Panel Rey</title>
</head>
<body>
<?php include('sidebar.php'); ?>
<div class="supercont">

	<?php include('header.php'); ?>
	<div class="linea"></div>
	<div class="casa-space"></div>
	<!-- <div class="banner-casa wow fadeIn" data-wow-delay=".2s">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 text-center">
					<h1><span>Residencia Panel Rey</span></h1>
					<small>Palo Blanco I</small>
				</div>
			</div>
		</div>
	</div> -->

	<div class="banner-casa">
		<div class="cycle-slideshow" data-cycle-fx="fade" data-cycle-slides="> div" data-cycle-timeout="5000" data-cycle-pager=".cycle-pager">
		    <div class="slide" style="background-image: url('assets/images/banner-casa.jpg');">
		    	<div class="caption">
		    		<div class="container">
		    			<div class="row">
		    				<div class="col-xs-12 text-center">
								<h1><span>Residencia Panel Rey</span></h1>
								<small>Palo Blanco I</small>
							</div>
		    			</div>
		    		</div>
		    	</div>
		    </div>
		    <!-- banner 2 -->
		    <div class="slide" style="background-image: url('assets/images/banner-casa2.jpg');">
		    	<div class="caption">
		    		<div class="container">
		    			<div class="row">
		    				<div class="col-xs-12 text-center">
								<h1><span>Residencia Panel Rey</span></h1>
								<small>Palo Blanco I</small>
							</div>
		    			</div>
		    		</div>
		    	</div>
		    </div>
		    <!-- banner 3 -->
		    <div class="slide" style="background-image: url('assets/images/banner-casa3.jpg');">
		    	<div class="caption">
		    		<div class="container">
		    			<div class="row">
		    				<div class="col-xs-12 text-center">
								<h1><span>Residencia Panel Rey</span></h1>
								<small>Palo Blanco I</small>
							</div>
		    			</div>
		    		</div>
		    	</div>
		    </div>
		    <!-- banner 4 -->
		    <div class="slide" style="background-image: url('assets/images/banner-casa4.jpg');">
		    	<div class="caption">
		    		<div class="container">
		    			<div class="row">
		    				<div class="col-xs-12 text-center">
								<h1><span>Residencia Panel Rey</span></h1>
								<small>Palo Blanco I</small>
							</div>
		    			</div>
		    		</div>
		    	</div>
		    </div>
		</div>
		<div class="cycle-pager"></div>
	</div>

	<section class="casa-intro wow fadeIn" data-wow-delay=".2s">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-6">
					<article>
						<h2>Sean bienvenidos a la <br>Residencia PANEL REY - PALO BLANCO I</h2>
						<p>Una residencia única en su tipo que combina de una manera impecable la modernidad de los más avanzados sistemas de construcción y sus materiales, con el esfuerzo delicadamente logrado por mantener una tradicional calidez de hogar para sus habitantes.</p>
						<p>Sus espacios han sido meticulosamente creados para generar un sentimiento acogedor que abraza inmediatamente a quienes conviven dentro y fuera de ella, como abriéndoles su corazón y volvién- dose de pronto parte de su vida.</p>
						<a href="assets/descargas/Folleto_ResidenciaPaloBlanco.pdf" target="_blank" class="descargar-b">Descargar Brochure</a>
					</article>
					<hr>
					<div class="row feats">
						<div class="col-xs-6 col-sm-4 col-md-6">
							<figure>
								<img src="assets/images/area-construccion.png" width="53" height="40" alt="Área de construcción" class="wow flipInY" data-wow-delay=".2s">
							</figure>
							<h3>Área de construcción</h3>
							<p>PB 456 / PA 354 / Total 811 m2</p>
						</div>
						<div class="col-xs-6 col-sm-4 col-md-6">
							<figure>
								<img src="assets/images/ubicacion.png" width="43" height="40" alt="Ubicación" class="wow flipInY" data-wow-delay=".2s">
							</figure>
							<h3>Ubicación</h3>
							<p>Manuel Doblado 860 Col. Palo Blanco San Pedro Garza García</p>
						</div>
						<div class="col-xs-6 col-sm-4 col-md-6">
							<figure>
								<img src="assets/images/diseno.png" width="44" height="40" alt="Diseño" class="wow flipInY" data-wow-delay=".2s">
							</figure>
							<h3>Diseño</h3>
							<p>Arq. Bernardo Pozas / POZAS Arquitectos</p>
						</div>
					</div>
				</div>
				<div class="col-sm-12 col-md-6">
					<!-- <figure class="thumb">
						<img src="assets/images/casa-panel-thumb.jpg" width="558" height="401" alt="Casa Panel Rey Palo Blanco I" class="wow fadeIn" data-wow-delay=".2s">
					</figure> -->
					<div class="embed-responsive embed-responsive-16by9">
						<div class="covervid-wrapper">
						    <video class="covervid-video" muted autoplay loop poster="assets/images/banner-video-pb.jpg">
						        <source src="assets/videos/panel-rey-palo-blanco.webm" type="video/webm">
						        <source src="assets/videos/panel-rey-palo-blanco.mp4" type="video/mp4">
						        <source src="assets/videos/panel-rey-palo-blanco.OGV" type="video/ogv">
						    </video>
						</div>
					</div>
					<blockquote class="panel-quote">
						Residencia única que combina la modernidad de los más avanzados sistemas de construcción.
					</blockquote>
				</div>
			</div>
		</div>
	</section>

	<section class="casa-nivel">
		<div class="container">
			<div class="row nivel">
				<article class="col-sm-12 col-md-4">
					<h4>Nivel 1</h4>
					<p>En la planta baja, su recepción que casi murmura un saludo cordial, encontra- mos un área de convivencia en la sala comedor y el bar, un espacio de Home Theater, la cocina-desayunador en donde se construyen muchas de las historias más memorables de las familias, un cuarto de servicio, lavandería, cochera cuádruple y una terraza y patio que invitan a convidar a amigos y familiares a deleitarse con su exquisito diseño.</p>
				</article>
				<div class="col-sm-12 col-md-8">
					<figure class="plano">
						<a href="assets/images/nivel1.jpg" class="wow fadeIn" data-wow-delay=".2s" title="Nivel 1">
							<img src="assets/images/nivel1.jpg" width="759" height="367" alt="Nivel 1" class="img-responsive center-block">
							<span><img src="assets/images/plus-naranja.png" width="39" height="39" alt="Zoom" title="Ver más grande"></span>
						</a>
					</figure>
					<div class="row feats">
						<div class="col-xs-6 col-sm-4 text-center">
							<figure>
								<img src="assets/images/cochera.png" alt="Cochera cuádruple" width="39" height="30">
							</figure>
							<figcaption>
								<span>1.</span> Cochera cuádruple
							</figcaption>
						</div>
						<div class="col-xs-6 col-sm-4 text-center">
							<figure>
								<img src="assets/images/sala-comedor.png" alt="Sala - Comedor - Bar" width="108" height="35">
							</figure>
							<figcaption>
								<span>2.</span> Sala - Comedor - Bar
							</figcaption>
						</div>
						<div class="col-xs-6 col-sm-4 text-center">
							<figure>
								<img src="assets/images/home-theater.png" alt="Home Theater" width="60" height="40">
							</figure>
							<figcaption>
								<span>3.</span> Home Theater
							</figcaption>
						</div>
						<div class="col-xs-6 col-sm-4 text-center">
							<figure>
								<img src="assets/images/cocina-desayunador.png" alt="Cocina - Desayunador" width="53" height="52">
							</figure>
							<figcaption>
								<span>4.</span> Cocina - Desayunador
							</figcaption>
						</div>
						<div class="col-xs-6 col-sm-4 text-center">
							<figure>
								<img src="assets/images/cuarto-servicio.png" alt="Cuarto de servicio" width="33" height="45">
							</figure>
							<figcaption>
								<span>5.</span> Cuarto de servicio
							</figcaption>
						</div>
						<div class="col-xs-6 col-sm-4 text-center">
							<figure>
								<img src="assets/images/terraza-patio.png" alt="Terraza y Patio" width="51" height="40">
							</figure>
							<figcaption>
								<span>6.</span> Terraza y Patio
							</figcaption>
						</div>
					</div>
				</div>
			</div>
			<hr>
			<div class="row nivel">
				<article class="col-sm-12 col-md-4">
					<h4>Nivel 2</h4>
					<p>En la planta alta, la intimidad de un family room que ofrece confort y comodi- dad, la recámara principal y tres habitaciones adicionales que cuentan con baño vestidor, para guardar el descanso pleno del resto de la familia.</p>
					<blockquote class="panel-quote">
						Suficiente amplitud en sus espacios para generar una sensación de tranquilidad inmediata.
					</blockquote>
				</article>
				<div class="col-sm-12 col-md-8">
					<figure class="plano">
						<a href="assets/images/nivel2.jpg" class="wow fadeIn" data-wow-delay=".2s" title="Nivel 2">
							<img src="assets/images/nivel2.jpg" width="755" height="503" alt="Nivel 2" class="img-responsive center-block">
							<span><img src="assets/images/plus-naranja.png" width="39" height="39" alt="Zoom" title="Ver más grande"></span>
						</a>
					</figure>
					<div class="row feats">
						<div class="col-xs-6 col-sm-4 text-center">
							<figure>
								<img src="assets/images/family-room.png" alt="Family Room" width="40" height="40">
							</figure>
							<figcaption>
								<span>7.</span> Family Room
							</figcaption>
						</div>
						<div class="col-xs-6 col-sm-4 text-center">
							<figure>
								<img src="assets/images/recamara.png" alt="Recámara principal y tres más con baño vestidor" width="84" height="41">
							</figure>
							<figcaption>
								<span>8.</span> Recámara principal y tres más con baño vestidor
							</figcaption>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="casa-alianzas wow fadeIn" data-wow-delay=".2s">
		<div class="container-fluid">
			<article class="row">
				<div class="col-xs-12 col-sm-12 col-md-12">
					<h5>Aliados</h5>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-3 item">
							<figure><img src="assets/images/lozzani.png" height="99" width="150" align="Lozzani Cocinas" title="Lozzani Cocinas"></figure>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-3 item">
							<figure><img src="assets/images/lampc.png" height="99" width="150" align="Lampc" title="Lampc"></figure>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-3 item">
							<figure><img src="assets/images/arca.png" height="68" width="300" align="Marmoles Arca" title="Marmoles Arca"></figure>
						</div>
						
						<div class="col-xs-12 col-sm-6 col-md-3 item">
							<figure><img src="assets/images/cristrq.png" height="99" width="150" align="Cristarq" title="Cristarq"></figure>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-3 item">
							<figure><img src="assets/images/sensa.png" height="99" width="150" align="Sensa" title="Sensa"></figure>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-3 item">
							<figure><img src="assets/images/moderna.png" height="68" width="300" align="Moderna" title="Moderna"></figure>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-3 item">
							<figure><img src="assets/images/accents.png" height="125" width="122" align="Accents" title="Accents"></figure>
						</div>
					</div>
				</div>
			</article>
		</div>
	</section>

	<section class="casa-arq">
		<div class="container">
			<article class="row">
				<div class="col-sm-12 col-md-6">
					<h6>Arquitecto Bernardo Pozas</h6>
					<p>Bernardo Pozas es originario de Monterrey, Nuevo León y egresado de la carrera Arquitectura del Tecnológico de Monterrey. Al salir al campo de trabajo, fundó un estudio de visualización arquitectónica en 3D llamado ARKIMEDIA. Actualmente dirige Pozas Arquitectos, un despacho dedicado al diseño residencial, comercial e interiorismo.</p>
					<p>Decidir su profesión nunca estuvo en duda, “ser arquitecto no fue una decisión consiente, la arquitectura me apasiona desde que tengo uso de razón.”</p>
					<p>La arquitectura de Bernardo Pozas busca la atemporalidad y la relevancia, logrando en su obra la trascendencia a través del tiempo. Cree firmemente en que la buena arquitectura no debe de ser presuntuosa, sino que debe de comprobar y ganarse cualquier mérito.</p>
					<p>Su arquitectura no tiene un “estilo” definido, sino que define la arquitectura y la tipología de sus obras como idiomas, por lo que aspira a ser políglota: hablar diferentes lenguas y expresarse en cuantiosas tipologías; siempre guardando la esencia de la arquitectura que lo caracteriza: con personalidad, carácter y atemporalidad.</p>
					<p>Con la visión de desarrollar proyectos del más alto nivel tanto en México como internacionalmente; innovando constantemente y ofreciendo una visión única, Pozas Arquitectos se enfoca en crear soluciones arquitectónicas y de diseño que vayan de acuerdo al entorno y cumplan con las altas expectativas de sus clientes, poniendo siempre al frente sus necesidades.</p>
					<p>El equipo de trabajo constituido por más de 50 jóvenes arquitectos trabajan con un compromiso y pasión creando no solamente arquitectura, sino una cultura de trabajo.</p>
				</div>
				<div class="col-sm-12 col-md-6">
					<figure class="wow fadeIn" data-wow-delay=".2s">
						<img src="assets/images/bernardo-pozas.jpg" alt="Arquitecto Bernardo Pozas" width="192" height="192" class="img-circle img-thumbnail center-block">
					</figure>
					<blockquote class="panel-quote">
						La arquitectura no se puede concebir en absolutos, debe ser dinámica y flexible.
						<small>Bernardo Pozas</small>
					</blockquote>
				</div>
			</article>
		</div>
	</section>


	<?php include('footer.php'); ?>

</div> <!-- cierra super content -->
<script src="assets/js/jquery.cycle2.min.js"></script>
<script src="assets/js/jquery.magnific-popup.min.js"></script>
<script src="assets/js/covervid.min.js"></script>
<script src="assets/js/center.js"></script>
<script>
$(document).ready(function() {
	$('.casa-alianzas .item img').center();

	$('.plano').magnificPopup({
		delegate: 'a', // child items selector, by clicking on it popup will open
		type: 'image',
		mainClass: 'mfp-with-zoom', // this class is for CSS animation below
		zoom: {
		    enabled: true, // By default it's false, so don't forget to enable it

		    duration: 300, // duration of the effect, in milliseconds
		    easing: 'ease-in-out', // CSS transition easing function

		    // The "opener" function should return the element from which popup will be zoomed in
		    // and to which popup will be scaled down
		    // By defailt it looks for an image tag:
		    opener: function(openerElement) {
		      // openerElement is the element on which popup was initialized, in this case its <a> tag
		      // you don't need to add "opener" option if this code matches your needs, it's defailt one.
		      return openerElement.is('img') ? openerElement : openerElement.find('img');
		    }
		}

	});
});
</script>

<?php include('bottom.php'); ?>

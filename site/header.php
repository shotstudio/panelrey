<header>

	<div class="container">
		<div class="row">
			<div class="col-xs-6 col-sm-2 col-md-2 logo">
				<a class="navbar-brand" href=".">
					<img class="hidden-xs hidden-sm hidden-md" src="assets/images/panel-rey-logotipo.png" height="80" width="158" alt="Panel Rey"/>
					<img class="visible-xs visible-sm visible-md" src="assets/images/logotipo-panelrey-footer.png" alt="Panel Rey"/>
				</a>
			</div>
			<div class="col-sm-10 col-md-10 hidden-xs ">
				<div class="info">
					<a href="tel:(81)83450055"><span>¡Llámanos!</span> 01 800 PANEL REY</a>
					<a href="mailto:residencialpr@gpromax.com"><i class="fa fa-envelope"></i>residencialpr@gpromax.com</a>
				</div>
			</div>
			<div class="col-xs-6 visible-xs mob-btn">
				<a class="nav-btn">
					<!-- <span>Menu</span> -->
					<i class="fa fa-bars"></i>
				</a>
			</div>
		</div>
	</div>

	<div class="f-menu">
		<div class="container">
			<div class="row">
				<div class="col-md-12 hidden-xs">
					<nav>
						<ul>
							<li><a href="nosotros.php">Nosotros</a></li>
							<li><a href="sistema-constructivo.php">Sistema Constructivo</a></li>
							<li><a href="galeria.php">Galería</a></li>
							<li><a href="residencia-panel-rey.php">Residencia</a></li>
							<li><a href="servicios.php">Servicios</a></li>
							<li><a href="videos.php">Videos</a></li>
							<li><a href="beneficios.php">Beneficios</a></li>
							<li><a href="leed.php">LEED</a></li>
							<li><a href="contacto.php">Contacto</a></li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</div>

</header>

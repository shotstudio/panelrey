
<?php include('top.php'); ?>
	<title>Panel Rey | Nosotros </title>
</head>
<body>
<?php include('sidebar.php'); ?>
<div class="supercont">

	<?php include('header.php'); ?>

	<div class="banner banner-nosotros">
		<div class="caption">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-6 col-md-offtet-* texto-banner">
						<h1>Nosotros</h1>
					</div>
				</div>
			</div>
		</div>
	<!-- 	<figure>
			<img src="images/banner-nosotros-panelrey.jpg" height="375" width="1400">
		</figure> -->
	</div>

	<div class="nosotros-historia">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12">
					<h2>Empresa mexicana con visión global, comprometidos con la excelencia, innovación, creatividad y la mejora continua como forma de vida. <span>Te ofrecemos un avanzado sistema integral de construcción para cada uno de tus proyectos.</span></h2>
				</div>
			</div>
			<div class="row item">
				<div class="col-xs-12 col-sm-6 col-md-6 col-md-6">
					<figure class="img-der" ><img class="wow fadeInUp" src="assets/images/panel-rey-1986.png" height="279" width="348" alt="Panel Rey 1986"></figure>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-md-6">
					<p>Inicia la operación de la planta Panel Rey, en El Carmen, N. L., con canteras propias de donde extrae la roca de yeso de la más alta pureza.</p>
				</div>
			</div>

			<div class="row item">
				<div class="hidden-xs col-sm-6 col-md-6 col-md-6">
					<p>Para garantizar el uso de materia prima de la más alta calidad en la elaboración de nuestros productos, Zinc Nacional, empresa hermana de Panel Rey, <span>inicia la operación de la Planta Cartoncillo.</span> El objetivo principal es el suministro de papel para la elaboración de paneles de yeso.</p>
				</div>
				<div class="hidden-xs col-sm-6 col-md-6 col-md-6">
					<figure><img  class="wow fadeInUp" src="assets/images/panel-rey-1999.png" height="281" width="367" alt="Panel Rey 1999"></figure>
				</div>
			</div>

			<!-- Version phone -->

			<div class="row item">
				<div class="col-xs-12 visible-xs">
					<figure><img  class="wow fadeInUp" src="assets/images/panel-rey-1999.png" height="281" width="367" alt="Panel Rey 1999"></figure>
				</div>
				<div class="col-xs-12 visible-xs">
					<p>Para garantizar el uso de materia prima de la más alta calidad en la elaboración de nuestros productos, Zinc Nacional, empresa hermana de Panel Rey, <span>inicia la operación de la Planta Cartoncillo.</span> El objetivo principal es el suministro de papel para la elaboración de paneles de yeso.</p>
				</div>
			</div>

			<div class="row item">
				<div class="col-xs-12 col-sm-6 col-md-6 col-md-6">
					<figure class="img-der"><img class="wow fadeInUp" src="assets/images/panel-rey-2003.png" height="279" width="347" alt="Panel Rey 2003"></figure>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-md-6">
					<p>Al exceder las normas ASTM, <span>nuestros paneles de yeso nos colocan como líderes en el mercado nacional y a exportar con éxito</span> a Estados Unidos, Canadá, Centro y Sudamérica.</p>
					<p>Por medio de una visión clara y experiencia hemos podido ver los tiempos y las necesidades en la industria de la construcción.</p>
				</div>
			</div>

			<div class="row item">
				<div class="hidden-xs col-sm-6 col-md-6 col-md-6">
					<p>Como resultado del liderazgo, la experiencia  y el éxito de nuestro sistema constructivo, se ve reflejada la preferencia de nuestros clientes, lo cual nos lleva a dar un paso más, con la <span>apertura de la tercera línea de producción, en San Luis Potosí,</span> implementando los más altos estándares a nivel mundial. La innovadora maquinaria automatizada y los más estrictos controles de calidad y procesos reafirman a Panel Rey como líder y como el mayor productor de paneles de yeso en Latinoamérica. <span>Seguimos construyendo el liderazgo.</span></p>
				</div>
				<div class="hidden-xs col-sm-6 col-md-6 col-md-6">
					<figure><img class="wow fadeInUp" src="assets/images/panel-rey-2013.png" height="279" width="367" alt="Panel Rey 2013"></figure>
				</div>
			</div>

			<!-- Version phone -->
			<div class="row item">
				<div class="col-xs-12 visible-xs">
					<figure><img class="wow fadeInUp" src="assets/images/panel-rey-2013.png" height="279" width="367" alt="Panel Rey 2013"></figure>
				</div>
				<div class="col-xs-12 visible-xs">
					<p>Como resultado del liderazgo, la experiencia  y el éxito de nuestro sistema constructivo, se ve reflejada la preferencia de nuestros clientes, lo cual nos lleva a dar un paso más, con la <span>apertura de la tercera línea de producción, en San Luis Potosí,</span> implementando los más altos estándares a nivel mundial. La innovadora maquinaria automatizada y los más estrictos controles de calidad y procesos reafirman a Panel Rey como líder y como el mayor productor de paneles de yeso en Latinoamérica. <span>Seguimos construyendo el liderazgo.</span></p>
				</div>
			</div>

			<div class="row item">
				<div class="col-xs-12 col-sm-6 col-md-6 col-md-6">
					<figure class="img-der"><img class="wow fadeInUp" src="assets/images/panel-rey-2015.png" height="281" width="348" alt="Panel Rey 2015"></figure>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-md-6">
					<p>Panel Rey es premiada como la mejor compañía del año por Global Gypsum a nivel internacional.</p>
				</div>
			</div>

			<div class="row item">
				<div class="hidden-xs col-sm-6 col-md-6 col-md-6">
					<p>Se empieza la construcción de nueva planta de producción de paneles de yeso en Cd. Juárez Chihuahua. </p>
				</div>
				<div class="hidden-xs col-sm-6 col-md-6 col-md-6">
					<figure><img class="wow fadeInUp" src="assets/images/panel-rey-2016.png" height="279" width="367" alt="Panel Rey 2016"></figure>
				</div>
			</div>

			<!-- Version phone -->
			<div class="row item">
				<div class="col-xs-12 visible-xs">
					<figure><img class="wow fadeInUp" src="assets/images/panel-rey-2016.png" height="279" width="367" alt="Panel Rey 2016"></figure>
				</div>
				<div class="col-xs-12 visible-xs">
					<p>Se empieza la construcción de nueva planta de producción de paneles de yeso en Cd. Juárez Chihuahua. </p>
				</div>
			</div>

		</div>
	</div>

	<div class="nosotros-lideres">
		<div class="container wow zoomIn">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<h3>Líderes en el Mercado Nacional y exportación</h3>
					<p>Al exceder las normas ASTM, nuestros paneles de yeso nos colocan como líderes en el mercado nacional y a exportar con éxito a Estados Unidos, Canadá, Centro y Sudamérica.</p>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<figure><img src="assets/images/logo-astm.png" height="162" width="148" alt="ASTM"></figure>
				</div>
			</div>
		</div>
	</div>

	<div class="nosotros-video">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<div class="embed-responsive embed-responsive-16by9">
				        <div class="covervid-wrapper">
				            <video controls poster="assets/images/video-nosotros.jpg" >
				                <source src="assets/video/panel-rey-conocenos.webm" type="video/webm">
				                <source src="assets/video/panel-rey-conocenos.mp4" type="video/mp4">
				                <source src="assets/video/panel-rey-conocenos.OGV" type="video/ogv">
				            </video>
				        </div>
				    </div>
				</div>
			</div>
		</div>
	</div> 

	<div class="nosotros-conocenos">
		<div class="container">
			<div class="row wow zoomIn">
				<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12">
					<h3>Conoce nuestro Sistema Constructivo </h3>
					<p>Te invitamos a navegar en nuestro Sistema Constructivo para que conozcas todas las soluciones que Panel Rey tiene para ti.</p>
					<a class="btn btn-naranja" href=".">Descubre más</a>
				</div>
			</div>
		</div>
	</div>
		
	

	<?php include('footer.php'); ?>
</div> <!-- cierra super content -->
<script src="assets/js/min/covervid.min.js"></script>

<?php include('bottom.php'); ?>
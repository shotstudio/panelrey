<?php include('top.php'); ?>
	<title>Panel Rey | Galería</title>
</head>
<body>
<?php include('sidebar.php'); ?>
<div class="supercont">

	<?php include('header.php'); ?>
	<div class="linea"></div>

<div class="galeria">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 intro-galeria">
				<h1>Galería de proyectos</h1>
				<p>A continuación te presentamos algunos de nuestros proyectos residenciales.</p>
			</div>
		</div>

	   	<div class="row img-galeria grid2 ">
	   		
	   		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 item2">
	   		 	<a href="aticos-puerta-hierro.php">
	    		<figure><img src="assets/images/galeria-thumb/aticos-thumb.jpg" height="260" width="360" alt="Áticos en Puerta de Hierro Monterrey"></figure>
	    		<h4>Áticos en Puerta de Hierro Monterrey</h4>
               	<span>Ver más</span>
	    		</a>
		    </div>
		    
		    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 item2">
		    	<a href="residencia-canterias.php">
		    	<figure><img src="assets/images/galeria-thumb/casa2-thumb.jpg" height="260" width="360" alt="Residencia en Canterías Monterrey"></figure>
		    	<h4>Residencia en Canterías Monterrey</h4>
		    	<span>Ver más</span>
	    		</a>
		    </div>
		    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 item2">
		    	<a href="residencia-glr.php">
		    	<figure><img src="assets/images/galeria-thumb/casa-glr-thumb.jpg" height="260" width="360" alt="Residencia GLR"></figure>
		    	<h4>Residencia GLR</h4>
		    	<span>Ver más</span>
	    		</a>
		    </div>

		    <!-- R O W 2 -->
	   		
	   		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 item2">
	   		 	<a href="residencia-hermosillo.php">
	    		<figure><img src="assets/images/galeria-thumb/casa-hermosillo-thumb.jpg" height="260" width="360" alt="Residencia en Hermosillo"></figure>
	    		<h4>Residencia en Hermosillo</h4>
	    		<span>Ver más</span>
	    		</a>
		    </div>
		    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 item2">
		    	<a href="residencia-LeNoir.php">
		    	<figure><img src="assets/images/galeria-thumb/casa-leNoir-thumb.jpg" height="260" width="360" alt="Residencia Le Noir"></figure>
		    	<h4>Residencia Le Noir</h4>
		    	<span>Ver más</span>
	    		</a>
		    </div>
		    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 item2">
		    	<a href="residencia-palo-blanco.php">
		    	<figure><img src="assets/images/galeria-thumb/casa-plobco-thumb.jpg" alt="Residencia Palo Blanco"></figure>
		    	<h4>Residencia Palo Blanco</h4>
		    	<span>Ver más</span>
	    		</a>
		    </div>

		    <!-- R O W 3 -->
			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 item2">
		    	<a href="residencia-tec-1.php">
		    	<figure><img src="assets/images/galeria-thumb/casatec-196-thumb.jpg" height="260" width="360" alt="Residencial Tec"></figure>
		    	<h4>Residencia Tec</h4>
		    	<span>Ver más</span>
	    		</a>
		    </div>
		    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 item2">
	   		 	<a href="residencia-tec-2.php">
	    		<figure><img src="assets/images/galeria-thumb/casatec-2010-thumb.jpg" height="260" width="360" alt="Residencial Tec"></figure>
	    		<h4>Residencia Tec</h4>
	    		<span>Ver más</span>
	    		</a>
		    </div>
		    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 item2">
		    	<a href="residencia-tec-3.php">
		    	<figure><img src="assets/images/galeria-thumb/casatec-2013-thumb.jpg" height="260" width="360" alt="Residencial Tec"></figure>
		    	<h4>Residencia Tec</h4>
		    	<span>Ver más</span>
	    		</a>
		    </div>

		    <!-- R O W 4 -->

			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 item2">
		    	<a href="residencia-tec-4.php">
		    	<figure><img src="assets/images/galeria-thumb/casatec-2014-thumb.jpg" height="260" width="360" alt="Residencial Tec"></figure>
		    	<h4>Residencia Tec</h4>
		    	<span>Ver más</span>
	    		</a>
		    </div>
		    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 item2">
		    	<a href="residencia-tec-5.php">
		    	<figure><img src="assets/images/galeria-thumb/casatec-2015-thumb.jpg" height="260" width="360" alt="Residencial Tec"></figure>
		    	<h4>Residencia Tec</h4>
		    	<span>Ver más</span>
	    		</a>
		    </div>
		    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 item2">
		    	<a href="residencia-tec-6.php">
		    	<figure><img src="assets/images/galeria-thumb/casatec-junio-thumb.jpg" height="260" width="360" alt="Residencial Tec"></figure>
		    	<h4>Residencia Tec</h4>
		    	<span>Ver más</span>
	    		</a>
		    </div>

		    <!-- R O W 5 -->
		    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 item2">
		    	<a href="residencia-tec-7.php">
		    	<figure><img src="assets/images/galeria-thumb/casatec-7-thumb.jpg" alt="Residencial Tec"></figure>
		    	<h4>Residencia Tec</h4>
		    	<span>Ver más</span>
	    		</a>
		    </div>

		    <!-- <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 item2">
		    	<a href="residencia-tec-8.php">
		    	<figure><img src="assets/images/galeria-thumb/casatec-2016-thumb.jpg" alt="Residencial Tec"></figure>
		    	<h4>Residencia Tec</h4>
		    	<span>Ver más</span>
	    		</a>
		    </div> -->

	   	</div>
	</div>
</div>

		
	

	<?php include('footer.php'); ?>
</div> <!-- cierra super content -->

<script src="assets/js/min/galeria-min.js"></script>
<?php include('bottom.php'); ?>
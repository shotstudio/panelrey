
<?php include('top.php'); ?>
<title>Panel Rey | Inicio</title>
</head>
<body>
<?php include('sidebar.php'); ?>
<div class="supercont">

	<?php include('header.php'); ?>

	<div class="home-slider">
		<div class="cycle-slideshow" data-cycle-fx="fade" data-cycle-slides="> div" data-cycle-timeout="5000" data-cycle-pager=".cycle-pager">
		    <div class="slide" style="background-image: url('assets/images/banner-inicio1.jpg');">
		    	<div class="caption">
		    		<div class="container">
		    			<div class="row">
		    				<div class="col-xs-12 col-md-8 col-md-offset-*">
		    					<h1>Construye o remodela justo lo que imaginas</h1>
		    					<p>Con Panel Rey Residencial tienes una opción flexible para<br />
		    					transformar tus espacios o crear el hogar que quieres.
		    					</p>
		    					<a href="servicios.php" class="btn btn-naranja">Descubre más</a>
		    				</div>
		    			</div>
		    		</div>
		    	</div>
		    </div>
		    <!-- banner 2 -->
		    <div class="slide" style="background-image: url('assets/images/banner-inicio2.jpg');">
		    	<div class="caption">
		    		<div class="container">
		    			<div class="row">
		    				<div class="col-xs-12 col-md-6 col-md-offset-*">
		    					<h1>Construye con rapidez</h1>
		    					<p>Por ser más fácil de instalar, reduce tiempos de trabajo<br />para que estrenes más rápido.</p>
		    					<a href="sistema-constructivo.php" class="btn btn-naranja">Descubre más</a>
		    				</div>
		    			</div>
		    		</div>
		    	</div>
		    </div>
		    <!-- banner 3 -->
		    <div class="slide" style="background-image: url('assets/images/banner-inicio3.jpg');">
		    	<div class="caption">
		    		<div class="container">
		    			<div class="row">
		    				<div class="col-xs-12 col-md-6 col-md-offset-*">
		    					<h1>Mayor Limpieza</h1>
		    					<p>Reduce el uso de mezclas húmedas, por lo que te<br />puedes olvidar de los polvos y manchas en exceso.</p>
		    					<a href="sistema-constructivo.php" class="btn btn-naranja">Descubre más</a>
		    				</div>
		    			</div>
		    		</div>
		    	</div>
		    </div>
		    <!-- banner 4 -->
		    <div class="slide" style="background-image: url('assets/images/banner-inicio4.jpg');">
		    	<div class="caption">
		    		<div class="container">
		    			<div class="row">
		    				<div class="col-xs-12 col-md-6 col-md-offset-*">
		    					<h1>Bienvenido al confort</h1>
		    					<p>En Panel Rey Residencial es resistente al paso del calor y<br />del sonido, ideal para crear espacios más agradables.</p>
		    					<a href="galeria.php" class="btn btn-naranja">Descubre más</a>
		    				</div>
		    			</div>
		    		</div>
		    	</div>
		    </div>
		     <!-- banner 5 -->
		    <div class="slide" style="background-image: url('assets/images/banner-inicio5.jpg');">
		    	<div class="caption">
		    		<div class="container">
		    			<div class="row">
		    				<div class="col-xs-12 col-md-6 col-md-offset-*">
		    					<h1>Ahorro que se nota</h1>
		    					<p>Con el sistema de aislamiento térmico, disfruta de cuartos frescos <br />en verano y cálidos en invierno, además de mayor ahorro energético que se refleja en tu recibo.</p>
		    					<a href="sistema-constructivo.php" class="btn btn-naranja">Descubre más</a>
		    				</div>
		    			</div>
		    		</div>
		    	</div>
		    </div>

		</div>
		<div class="cycle-pager"></div>
	</div>

	<div class="intro">
		<div class="container">
			<div class="row text-intro wow fadeInUp">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<h2>Ingeniería para la construcción y remodelación residencial en interiores y exteriores.</h2>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<p>Somos una empresa mexicana con visión global, comprometidos con la excelencia, innovación, creatividad y la mejora continua como forma de vida. Te ofrecemos un avanzado sistema integral de construcción para cada uno de tus proyectos.</p>
				</div>
			</div>
	
			<div class="row home-servicios grid2">
		   		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 item wow fadeInUp">
		   		 	<a href="servicios.php">
		    		<figure><img src="assets/images/home-servicios.jpg" height="284" width="554" alt="Panel Rey Servicios"/></figure>
		    			<h3>Servicios</h3>
                   		<p>Tenemos hasta 10 servicios de ingeniería para tu proyecto.</p>
                   		<span>Descubre más</span>
		    		</a>
			    </div>
			    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 item wow fadeInUp">
			    	<a href="beneficios.php">
			    	<figure><img src="assets/images/home-beneficios.jpg" height="284" width="557" alt="Panel Rey Beneficios"/></figure>
			    		<h3>Beneficios</h3>
                   		<p>Conoce los beneficios que los productos PanelRey ofrecen.</p>
                   		<span>Descubre más</span>
		    		</a>
			    </div>
			    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 item wow fadeInUp">
			    	<a href="galeria.php">
			    	<figure><img class="hidden-xs" src="assets/images/home-proyectos.jpg" height="284" width="1139" alt="Panel Rey Proyectos"/></figure>
			    	<figure><img class="hidden-sm hidden-md hidden-lg" src="assets/images/home-proyectos-xs.jpg" height="284" width="554" alt="Panel Rey Proyectos"></figure>
			    		<h3>Proyectos</h3>
                   		<p>Visita nuestra galería de proyectos.</p>
                   		<span>Descubre más</span>
		    		</a>
			    </div>
			</div>
		</div>
	</div>

	<div class="home-redes">
		<div class="container">
			<div class="row">
				<div class="col-md-12 wow fadeInUp">
					<h2>¡Síguenos en nuestras redes sociales!</h2>
				</div>
			</div>
			<div class="row redes-section">
				<div class="col-xs-12 col-sm-6 col-md-4 redes-content redes-fb">
					<div id="fb-root"></div>
					<script>(function(d, s, id) {
					  var js, fjs = d.getElementsByTagName(s)[0];
					  if (d.getElementById(id)) return;
					  js = d.createElement(s); js.id = id;
					  js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.4&appId=322604911217598";
					  fjs.parentNode.insertBefore(js, fjs);
					}(document, 'script', 'facebook-jssdk'));</script>
					<div class="fb-page" data-href="https://www.facebook.com/PanelReyMX" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false">
					<div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/PanelReyMX"><a href="https://www.facebook.com/PanelReyMX">Panel Rey</a></blockquote></div></div>
				</div>

				<div class="col-xs-12 col-sm-6 col-md-4 redes-content">
					<a class="twitter-timeline" href="https://twitter.com/panelreymexico" data-widget-id="679073191579398146">Tweets por el @panelreymexico.</a>
					<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
				</div>

				<div class="col-xs-12 col-sm-6 col-md-4 redes-content">
					<script src="https://apis.google.com/js/platform.js"></script>
					<div class="g-ytsubscribe" data-channel="gpromax" data-layout="full" data-count="default"></div>
					
					<div class="embed-responsive embed-responsive-16by9">
						<iframe class="embed-responsive-item" width="300" height="200" src="http://www.youtube.com/embed?max-results=1&controls=0&showinfo=0&rel=0&listType=user_uploads&list=gpromax" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
						<script src="http://www.gmodules.com/ig/ifr?url=http://www.google.com/ig/modules/youtube.xml&up_channel=gpromax&synd=open&w=320&h=390&title=Título+A+Mostrar&border=&output=js"></script>
					</div>
				</div>
			</div>
		</div>
	</div>



	<?php include('footer.php'); ?>
</div> <!-- cierra super content -->

<script src="assets/js/jquery.cycle2.min.js"></script>

<?php include('bottom.php'); ?>
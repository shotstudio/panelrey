
<?php include('top.php'); ?>
	<title>lEED | Panel Rey</title>
</head>
<body>
<?php include('sidebar.php'); ?>
<div class="supercont">

	<?php include('header.php'); ?>

	<div class="banner banner-leed">
		<div class="caption">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-6 col-md-offtet-* texto-banner">
						<h1>LEED</h1>
						<p>Promovemos el desarrollo sustentable en la construcción.</p>
					</div>
				</div>
			</div>
		</div>
		<!-- <figure>
			<img src="images/banner-leed.jpg" height="375" width="1400">
		</figure> -->
	</div>

	<div class="leed">
		<div class="container wow fadeInUp">
			<div class="row">
				<div class="col-xs-12 col-xs-12 col-md-6 col-lg-6">
					<p><strong>LEED es una herramienta para edificios verdes que direcciona el ciclo de vida entera de un edificio reconociendo las mejores estrategias y a los mejores edificios en su clase.</strong> (LEED) Liderazgo en Energía & Diseño Ambiental, según las siglas en inglés, está transformando la manera en sobre como nuestros edificios y comunidades son diseñadas, edificadas, se les da mantenimiento y operan alrededor del mundo.</p>
				</div>
				<div class="col-xs-12 col-xs-12 col-md-6 col-lg-6">
					<p>Los sistemas de clasificación son grupos de requerimientos que debe cumplir los proyectos que quieren lograr la certificación LEED. Cada requerimiento es único y se orienta según las necesidades y el tipo de proyecto. La herramienta LEED es suficientemente flexible para aplicar a casi todos los tipos de proyectos de salud, de instalaciones, escuelas, hogares y hasta vecindarios completos.
					</p>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12 col-xs-12 col-md-6 col-lg-6">
					<p><strong>LEED es un programa de verificación para edificios sustentables por parte de un tercero (USGBC).</strong> Los proyectos de edificios deben cumplir con pre-requisitos y créditos en distintas categorías para obtener puntos y así lograr diferentes niveles de certificación. Existen distintos sistema de clasificación dependiendo el tipo de proyecto que se pretende certificar. 	
					</p>
				</div>
				<div class="col-xs-12 col-xs-12 col-md-6 col-lg-6">
					<figure><img src="assets/images/leed.png" height="94" width="374"></figure>
				</div>
			</div>
		</div>
	</div>

	<div class="leed-certificacion">
		<div class="container">
			<div class="row">
				<di class="col-xs-12 col-sm-12 col-md-4 col-lg-4 wow zoomIn">
					<figure><img src="assets/images/categorias-leed.svg" height="350" width="354"></figure>
				</di>
				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
					<h3>Niveles de Certificación</h3>
					<p>Dentro de cada una de las categorías de créditos LEED, los proyectos deben satisfacer los pre-requisitos y obtener puntos. El número de puntos que obtiene el proyecto determina el nivel de certificación LEED.</p>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
							<h4>Créditos de eficiencia de agua</h4>
							<p>Promueve un uso inteligente del agua, dentro y fuera, para reducir el consumo de agua potable. 
							</p>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
							<h4>Créditos de materiales y recursos</h4>
							<p>Fomenta el uso de materiales sustentables para el edificio y reducir el desperdicio. 
							</p>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
							<h4>Créditos de calidad ambiental interior</h4>
							<p>Promueve una mejor calidad del aire interior y acceso a la luz del dia asi y vistas. 
							</p>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
							<h4>Créditos de sitio sustentable</h4>
							<p>Fomenta estrategias que minimizan el impacto en un ecosistema y recursos de agua.
							</p>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>

	<div class="que-hace-leed">
		<div class="container">
			<div class="row wow fadeInUp">
				<div class="col-md-8 col-md-offset-4">
					<h3>¿Qué puede hacer LEED por ti?</h3>
					<p>El Consejo de Edificos Sustentables de EUA (USGBC) organización sin fines de lucro comprometida a un futuro prospero y sustentable para nuestra nacion a traves de los edficios rentables y ahorradores de energía es el organismo de certifica que el proyecto cumpla con todos los requerimientos necesarios para una certificación. </p>
					<ul>
						<li>Disminuir los costos de operación e incrementar el valor del capital </li>
						<li>Conservar la energia, agua y otros recursos </li>
						<li>Ser más saludable y seguro para sus ocupantes  </li>
						<li>Calificar para incentivos economicos ahorradores, como descuento en impuestos y subsidio de zonificacion </li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<?php include('footer.php'); ?>
</div> <!-- cierra super content -->

<?php include('bottom.php'); ?>

<?php include('top.php'); ?>
	<title>Panel Rey | Videos</title>
</head>
<body>
<?php include('sidebar.php'); ?>
<div class="supercont">

	<?php include('header.php'); ?>
	<div class="linea"></div>

<div class="galeria">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 intro-galeria">
				<h1>Galería de videos</h1>
				<p>Seleccione una categoría de videos para ver.</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 col-md-offset-*">
				<select class="filters-select form-control">
					<option value="*">-Seleccionar-</option>
					<option value=".residencial">Construcción Residencial</option>
					<option value=".glass-rey">Glass Rey</option>
					<option value=".constructivo">Sistema Constructivo</option>
					<option value=".video4">Instalaciones</option>
					<option value="*">Mostrar todos</option>
				</select>
			</div>
		</div>
	   	<div class="row grid-video">	   		
	   		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 item element-video residencial">
	   			<div class="caption">
	   		 		<a class="popup-youtube" href="https://www.youtube.com/watch?v=VAreHJkEzj4">
	    				<figure><img src="assets/images/video1.jpg" height="201" width="357" alt="Construcción Residencial"> </figure>
	    				<span>Construcción Residencial con sistema constructivo Panel Rey</span>
	    			</a>
		    	</div>
		    </div>

		    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 item element-video glass-rey">
	   			<div class="caption">
	   		 		<a class="popup-youtube" href="https://www.youtube.com/watch?v=8nJMBb0te2E">
	    				<figure><img src="assets/images/glass-rey.jpg" height="201" width="357" alt="Glass Rey"></figure>
	    				<span>Panel Rey Glass Rey</span>
	    			</a>
		    	</div>
		    </div>

		    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 item element-video constructivo">
	   			<div class="caption">
	   		 		<a class="popup-youtube" href="https://www.youtube.com/watch?v=2nKs9z8VR7U">
	    				<figure><img src="assets/images/sistema-constructivo.jpg" alt="Sistema Constructivo"></figure>
	    				<span>Sistema Constructivo Panel Rey en Restautante Italiano</span>
	    			</a>
		    	</div>
		    </div>

		    <!-- R O W 2 -->
	   		
	   		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 item element-video video4">
	   			<div class="caption">
	   		 		<a class="popup-youtube" href="https://www.youtube.com/watch?v=WnMeyERMhdQ">
	    				<figure><img src="assets/images/sistema-adherencia.jpg" height="201" width="357" alt="Sistema de Adherencia AD Panel"></figure>
	    				<span>Sistema de Adherencia AD Panel - Instalación</span>
	    			</a>
		    	</div>
		    </div>

		    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 item element-video video4">
	   			<div class="caption">
	   		 		<a class="popup-youtube" href="https://www.youtube.com/watch?v=TgUG5leo2jo">
	    				<figure><img src="assets/images/poste-fachada.jpg" height="201" width="357" alt="Poste Fachada"></figure>
	    				<span>Poste Fachada</span>
	    			</a>
		    	</div>
		    </div>

		    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 item element-video video4">
	   			<div class="caption">
	   		 		<a class="popup-youtube" href="https://www.youtube.com/watch?v=kijibiH4Kmc">
	    				<figure><img src="assets/images/muro-divisiorio.jpg" height="201" width="357" alt="Muro Divisorio"></figure>
	    				<span>Muro Divisorio - Panel Rey</span>
	    			</a>
		    	</div>
		    </div>

		    <!-- R O W 3 -->
  

	   	</div>
	</div>
</div>

	<?php include('footer.php'); ?>
</div> <!-- cierra super content -->

<script src="assets/js/min/video-min.js"></script>
<?php include('bottom.php'); ?>
<?php include('top.php'); ?>
	<title>Panel Rey | Galería</title>
</head>
<body>
<?php include('sidebar.php'); ?>
<div class="supercont">

	<?php include('header.php'); ?>
	<div class="linea"></div>

	<div class="sist-const">
		<div class="container">
			<div class="row">

				<!-- titulo -->
				<div class="col-lg-12 intro-sist">
					<h1>Sistema Constructivo</h1>
					<div class="col-xs-6">
						<p>Explora nuestros sistemas constructivos y descubre los materiales que hacen realidad tus proyectos.</p>
					</div>
					<div class="col-xs-6">
						<div class="media">
						  	<div class="media-left">
						    	<a href="#">
						      		<img class="media-object" src="assets/images/app-sistema.jpg" alt="...">
						   		</a>
						  	</div>
						  	<div class="media-body">
						   		<h4 class="media-heading">Descarga la App para Ipad</h4>
						    	<!-- <p>Esta aplicación te permite ver de manera simple y gráfica, cómo utilizar nuestros materiales para los fines de construcción que necesitas.</p> -->
						    	<a href="https://itunes.apple.com/mx/app/sistema-constructivo-panel/id945182764?mt=8" target="_blank"><i class="fa fa-download" aria-hidden="true"></i> Descargar </a>
						  	</div>
						</div>
					</div>
				</div>
				

				<!-- dropnavs -->
				<div class="col-xs-12">
					<ul class="nav nav-tabs sistema-links">
						<li><a href="#" data-link="assets/sc/casa.html"><i class="fa fa-home"></i> Inicio</a></li>
						<li class="dropdown">
							<a href="#" id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Sistemas para Exterior <span class="caret"></span>
							</a>
							<ul class="dropdown-menu" aria-labelledby="dLabel">
								<li><a href="#" data-link="assets/sc/muro-fachada.html">Muro Fachada Princial</a></li>
								<li><a href="#" data-link="assets/sc/muro-lateral.html">Muro Fachada Lateral</a></li>
								<li><a href="#" data-link="assets/sc/techo.html">Techo</a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a href="#" id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Sistemas para Interior <span class="caret"></span>
							</a>
							<ul class="dropdown-menu" aria-labelledby="dLabel">
								<li><a href="#" data-link="assets/sc/interior-sala.html">Muro Interior y Plafón Sala</a></li>
								<li><a href="#" data-link="assets/sc/interior-cocina.html">Muro Interior y Plafón Cocina</a></li>
								<li><a href="#" data-link="assets/sc/interior-bano.html">Muro Interior y Plafón Baño</a></li>
							</ul>
						</li>
					</ul>
				</div>

				<!-- carga -->
				<div class="col-xs-12">
					<!-- <div id="#loading" class="spinner"></div> -->
					<div id="contents" class="casa"></div>
				</div>

			</div>
		</div>
	</div>

	<?php include('footer.php'); ?>

</div> <!-- cierra super content -->

<script type="text/javascript">
$(document).ready(function() {
	var direccion = 'assets/sc/casa.html';

	function cargaSecciones(direccion){
		$('#contents').load(direccion);
	}

	cargaSecciones(direccion);

	$('.sistema-links a').click(function(e){
		var link = $(this).data('link');
		//console.log(link);
		cargaSecciones(link);
		e.preventDefault();
	});
});
</script>

<?php include('bottom.php'); ?>

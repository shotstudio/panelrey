
<?php include('top.php'); ?>
	<title>Panel Rey | Residencia Tec</title>
</head>
<body>
<?php include('sidebar.php'); ?>
<div class="supercont">

	<?php include('header.php'); ?>

<div class="linea"></div>
<div class="galeria-interior">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 intro-interior">
				<h1>Residencia Tec</h1>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<ol class="breadcrumb">
				  <li><a href="galeria.php">Galería</a></li>
				  <li>Residencia Tec</li>
				</ol>
			</div>
		</div>
	</div>
</div>

<div class="galeria-casa">
	<div class="gallery js-flickity" data-flickity-options='{ "freeScroll": true, "wrapAround": true }'>
		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-Tec-junio/1.jpg">
		</div>

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-Tec-junio/2.jpg" height="480" width="640">
		</div>

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-Tec-junio/3.jpg" height="480" width="640">
		</div> 

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-Tec-junio/4.jpg" height="480" width="640">
		</div> 

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-Tec-junio/5.jpg" height="480" width="640">
		</div> 

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-Tec-junio/6.jpg" height="480" width="640">
		</div> 

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-Tec-junio/7.jpg" height="480" width="640">
		</div> 

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-Tec-junio/8.jpg" height="480" width="640">
		</div> 

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-Tec-junio/9.jpg" height="480" width="640">
		</div> 

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-Tec-junio/10.jpg" height="480" width="640">
		</div> 

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-Tec-junio/11.jpg" height="480" width="640">
		</div> 

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-Tec-junio/12.jpg" height="480" width="640">
		</div> 

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-Tec-junio/13.jpg" height="480" width="640">
		</div> 

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-Tec-junio/14.jpg" height="480" width="640">
		</div> 

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa-Tec-junio/15.jpg" height="480" width="640">
		</div> 

	</div>
</div>

<div class="info-casa">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
				<ul>
					<li>Residencial del sorteo TEC de Monterrey construida con material Panel Rey en sus plafones corridos.</li>
					<li>Arquitecto Pablo Ferrara.</li>
				</ul>
			</div>
		</div>
	</div>
</div>

<iframe src="https://www.google.com/maps/embed?pb=!1m0!3m2!1sen!2smx!4v1456765835772!6m8!1m7!1sy5H8kzLtalHG-FBwj-HSVw!2m2!1d25.65120732314058!2d-100.3438773345054!3f43.225885391214035!4f-9.263765969104298!5f0.4786484563733288" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
		
	

	<?php include('footer.php'); ?>
</div> <!-- cierra super content -->

<script src="assets/js/flickity.pkgd.min.js"></script>

<?php include('bottom.php'); ?>

<?php include('top.php'); ?>
	<title>Panel Rey | Residencia Palo Blanco</title>
</head>
<body>
<?php include('sidebar.php'); ?>
<div class="supercont">

	<?php include('header.php'); ?>

<div class="linea"></div>
<div class="galeria-interior">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 intro-interior">
				<h1>Residencia Palo Blanco</h1>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<ol class="breadcrumb">
				  <li><a href="galeria.php">Galería</a></li>
				  <li>Residencia Palo Blanco</li>
				</ol>
			</div>
		</div>
	</div>
</div>

<div class="galeria-casa">
	<div class="gallery js-flickity" data-flickity-options='{ "freeScroll": true, "wrapAround": true }'>
		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/casa-plobco/1.jpg" height="480" width="640">
		</div>
		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/casa-plobco/2.jpg" height="480" width="640">
		</div>
		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/casa-plobco/3.jpg" height="480" width="640">
		</div>
		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/casa-plobco/4.jpg" height="480" width="640">
		</div>
		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/casa-plobco/5.jpg" height="480" width="640">
		</div>
		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/casa-plobco/6.jpg" height="480" width="640">
		</div> 
		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/casa-plobco/7.jpg" height="480" width="640">
		</div>
		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/casa-plobco/8.jpg" height="480" width="640">
		</div>
		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/casa-plobco/10.jpg" height="480" width="640">
		</div>
		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/casa-plobco/11.jpg" height="480" width="640">
		</div>
	</div>
</div>

<div class="info-casa">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
				<ul>
					<li>Hermosa residencia construida en su totalidad con el sistema constructivo Panel Rey, sus interiores, muros divisorios, plafones corridos y fachadas.</li>
					<li>Realizada por el Arquitecto Rodrigo de la Peña.</li>
				</ul>
			</div>
		</div>
	</div>
</div>

<iframe src="https://www.google.com/maps/embed?pb=!1m0!3m2!1sen!2smx!4v1452895590090!6m8!1m7!1sXle8uOQNI-hQxSzMFcBICQ!2m2!1d25.65302444699818!2d-100.3946527741769!3f204.9891098709577!4f2.9664240026997675!5f0.4000000000000002" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>	

	<?php include('footer.php'); ?>
</div> <!-- cierra super content -->
<script src="assets/js/flickity.pkgd.min.js"></script>


<?php include('bottom.php'); ?>
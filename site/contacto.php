
<?php include('top.php'); ?>
	<title>Contacto | Panel Rey</title>
</head>
<body>
<?php include('sidebar.php'); ?>
<div class="supercont">

	<?php include('header.php'); ?>
	<div class="linea"></div>
	<div class="contacto">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h1>Contacto</h1>
					<h2>Queremos resolver todas tus dudas, comentarios y ofrecerte nuevos servicios.</h2>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6 col-lg-6">
					<form class="form">
						<div class="row">
				            <div class="col-xs-12 col-sm-6 col-md-6">
				              <div class="form-group">
							    <input type="text" class="form-control" name="nombre" placeholder="Nombre y Apellido*">
							  </div>
				            </div>
				            <div class="col-xs-12 col-sm-6 col-md-6">
				             	<div class="form-group">
							    <input type="email" class="form-control" name="email" placeholder="Correo Electrónico*">
							  </div>
				            </div>
				        </div>
				        <div class="row">
				            <div class="col-xs-12 col-sm-6 col-md-6">
				              <div class="form-group">
							    <input type="text" class="form-control" name="telefono" placeholder="Teléfono">
							  </div>
				            </div>
				            <div class="col-xs-12 col-sm-6 col-md-6">
				             	<select class="form-control" placeholder="Estado" name="estado">
							    	<option value="*">Estado</option>
									<option value="Aguascalientes">Aguascalientes</option>
									<option value="Baja California">Baja California</option>
									<option value="Baja California Sur">Baja California Sur</option>
									<option value="Campeche">Campeche</option>
									<option value="Chiapas">Chiapas</option>
									<option value="Chihuahua">Chihuahua</option>
									<option value="Coahuila">Coahuila</option>
									<option value="Colima">Colima</option>
									<option value="Distrito Federal">Distrito Federal</option>
									<option value="Durango">Durango</option>
									<option value="Estado de México">Estado de México</option>
									<option value="Guanajuato">Guanajuato</option>
									<option value="Guerrero">Guerrero</option>
									<option value="Hidalgo">Hidalgo</option>
									<option value="Jalisco">Jalisco</option>
									<option value="Michoacán">Michoacán</option>
									<option value="Morelos">Morelos</option>
									<option value="Nayarit">Nayarit</option>
									<option value="Nuevo León">Nuevo León</option>
									<option value="Oaxaca">Oaxaca</option>
									<option value="Puebla">Puebla</option>
									<option value="Querétaro">Querétaro</option>
									<option value="Quintana Roo">Quintana Roo</option>
									<option value="San Luis Potosí">San Luis Potosí</option>
									<option value="Sinaloa">Sinaloa</option>
									<option value="Sonora">Sonora</option>
									<option value="Tabasco">Tabasco</option>
									<option value="Tamaulipas">Tamaulipas</option>
									<option value="Tlaxcala">Tlaxcala</option>
									<option value="Veracruz">Veracruz</option>
									<option value="Yucatán">Yucatán</option>
									<option value="Zacatecas">Zacatecas</option>
								</select>
				            </div>
				        </div>
				        <div class="row">
				            <div class="col-xs-12 col-sm-12 col-md-12">
							    <textarea name="mensaje" class="form-control" rows="1" placeholder="Mensaje"></textarea>
				            </div>
				        </div>
				        <div class="row">
				        	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				        		<div class="g-recaptcha" data-sitekey="6LeKIh0TAAAAAKGiOiZZ_wmbeVxGOWKs3AFyovfy"></div>
				        	</div>
				        	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				        		<button type="input" class="btn btn-naranja">Enviar</button>
				        	</div>
				        </div>
					</form>
				</div>

				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 contacto-info">
					<span>Llámanos</span>
					<p>Marca al <a href="">01 800 PANEL REY</a></p>
					<p>Envíanos un correo a <a href="mailto:residencialpr@gpromax.com">residencialpr@gpromax.com</a> para dudas o sugerencias </p>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 contacto-info">
					<span>Oficinas Corporativas</span>
					<p>Serafín Peña 935 Sur, Col. Centro C.P. 64000 El Carmen, Nuevo León</p>
					<a href="tel:(81)8183406455">T. (81) 8345 0055</a>
				</div>
			</div> <!-- cierra div row -->

		</div>
	</div>

<div id="map_canvas"></div>

<div class="msggracias">Gracias, tu mensaje fue enviado.</div>
<div class="msgnoenviado"></div>
<div class="msgnoenviadocaptcha">Error en Captcha, intenta de nuevo por favor.</div> 	

<script src='https://www.google.com/recaptcha/api.js'></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="assets/js/mapa.js"></script>
	<?php include('footer.php'); ?>
</div> <!-- cierra super content -->

<!-- jquery validate -->
<script src="assets/js/jquery.validate.js"></script>
<script src="assets/js/additional-methods.js"></script>
<script src="assets/js/validate.js"></script>

<?php include('bottom.php'); ?>
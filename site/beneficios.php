
<?php include('top.php'); ?>
	<title>Beneficios | Panel Rey </title>
</head>
<body>
<?php include('sidebar.php'); ?>
<div class="supercont">

	<?php include('header.php'); ?>

	<div class="banner banner-beneficios">
		<div class="caption">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-6 col-md-offtet-* texto-banner">
						<h1>Beneficios</h1>
					</div>
				</div>
			</div>
		</div>
		<!-- <figure>
			<img src="images/banner-beneficios-panel-rey.jpg" height="375" width="1400">
		</figure> -->
	</div>

	<div class="innovador">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-*">
					<h2>Más innovador, <strong>ingeniería para la construcción y remodelación residencial,</strong> en interiores y exteriores. </h2>
					<p>Con Panel Rey obtienes un sistema ligero y total para la construcción de viviendas en todo México, recibiendo asesoría profesional personalizada. Logra el mayor beneficio del desarrollo de ingenierías para la construcción completa de ida casa de hasta 3 niveles, remodelaciones, ampliaciones y detalles constructivos, tanto en interior como en exterior.</p>
					<ul>
						<li><img class="wow zoomIn" src="assets/images/i-limpieza.png" height="24" width="34">Limpieza en el proceso de la obra.</li>
						<li><img class="wow zoomIn" src="assets/images/i-estructural.png" height="42" width="49">Un sistema estructural más ligero (ahorro en cimentaciones).</li>
						<li><img class="wow zoomIn" src="assets/images/i-soluciones.png" height="42" width="60">Soluciones acústicas (gran confort).</li>
						<li><img class="wow zoomIn" src="assets/images/i-rapidez.png" height="42" width="35">Rapidez de construcción</li>
						<li><img class="wow zoomIn" src="assets/images/i-aislamiento.png" height="30" width="60">Aislamiento térmico (ahorro de energía).</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="beneficios-atico">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12 texto-intro">
					<h3 class="wow slideInUp">Áticos con Panel Rey</h3>
					<p>Panel Rey promueve la construcción de áticos con el sistema ligero y a demás la utilización de aislamiento, que en conjunto da como resultado áticos habitables que ofrecen a los hogares mayores espacios que pueden utilizar de esparcimiento.</p>
				</div>
			</div>
			<div class="row beneficios-lista">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<h4 class="wow slideInUp">Espacios habitables y confortables</h4>
					<p>Hogares más frescos en verano y más cálidos en invierno.</p>

					<h4 class="wow slideInUp">Ecológicos y sustentables</h4>
					<p>Gracias al aislamiento que se utiliza en la construcción de los áticos, se llega a ahorrar hasta un 30% de energía del uso de aire acondicionado y calefacción.</p>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<h4 class="wow slideInUp">Espacios grandes y excelente acabado</h4>
					<p>Diseños con buena estética y modernidad, ámplios espacios con altura para usos múltiples.</p>

					<h4 class="wow slideInUp">Resistentes y durables</h4>
					<p>Los áticos construidos con sistema Panel Rey son de alta resistencia y durabilidad.</p>
				</div>
			</div>
		</div>
	</div>
	
	<div class="banner-construye">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 text-center wow zoomIn">
					<h3>Construye inteligente, <br /> construye Panel Rey</h3> 
				</div>
			</div>
		</div>
	</div>
	


		
	

	<?php include('footer.php'); ?>
</div> <!-- cierra super content -->

<?php include('bottom.php'); ?>
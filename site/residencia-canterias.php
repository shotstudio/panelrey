
<?php include('top.php'); ?>
	<title>Panel Rey| Residencia en Canterías</title>
</head>
<body>
<?php include('sidebar.php'); ?>
<div class="supercont">

	<?php include('header.php'); ?>

<div class="linea"></div>
<div class="galeria-interior">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 intro-interior">
				<h1>Residencia en Canterías Monterrey</h1>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<ol class="breadcrumb">
				  <li><a href="galeria.php">Galería</a></li>
				  <li>Residencia en Canterías Monterrey</li>
				</ol>
			</div>
		</div>
	</div>
</div>

<div class="galeria-casa">
	<div class="gallery js-flickity" data-flickity-options='{ "freeScroll": true, "wrapAround": true }'>
		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa2/1.jpg" height="480" width="640">
		</div>

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa2/2.jpg" height="480" width="640">
		</div>

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa2/3.jpg" height="480" width="640">
		</div>

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa2/4.jpg" height="480" width="640">
		</div> 

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa2/5.jpg" height="480" width="640">
		</div> 

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa2/6.jpg" height="480" width="640">
		</div> 

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa2/7.jpg" height="480" width="640">
		</div> 

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa2/8.jpg" height="480" width="640">
		</div> 

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa2/9.jpg" height="480" width="640">
		</div>

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa2/10.jpg" height="480" width="640">
		</div>

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa2/11.jpg" height="480" width="640">
		</div>

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa2/12.jpg" height="480" width="640">
		</div>

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa2/13.jpg" height="480" width="640">
		</div>

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa2/14.jpg" height="480" width="640">
		</div>

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa2/15.jpg" height="480" width="640">
		</div>

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa2/16.jpg" height="480" width="640">
		</div>

		<div class="gallery-cell">
			<img src="assets/images/galeria-thumb/Casa2/17.jpg" height="480" width="640">
		</div>

	</div>
</div>

<div class="info-casa">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<ul>
					<li>Residencia conformada por estructuras metálicas y con el sistema constructivo Panel Rey.</li>
				</ul>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<ul>
					<li>Se pueden apreciar las diferentes etapas en la construcción en donde se observa como se mezclan los procesos constructivos siendo posible poder edificar una residencia en conjunto o hacerla al 100% con el sistema constructivo Panel Rey.</li>
				</ul>
			</div>
		</div>
	</div>
</div>

<iframe src="https://www.google.com/maps/embed?pb=!1m0!3m2!1sen!2smx!4v1450735593927!6m8!1m7!1si8b7fVbLKqx3S2jkJAolAw!2m2!1d25.65256977847384!2d-100.3957074249579!3f102.72531001933233!4f-2.6041803410657423!5f0.7820865974627469" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>	
	

	<?php include('footer.php'); ?>
</div> <!-- cierra super content -->
<script src="assets/js/flickity.pkgd.min.js"></script>

 
<?php include('bottom.php'); ?>
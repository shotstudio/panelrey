// @codekit-prepend  "jquery.magnific-popup.js", "imagesloaded.pkgd.min.js", "isotope.pkgd.min.js",

$(document).ready(function() {
  //pop galeria
  $('.popup-youtube').magnificPopup({
    disableOn: 700,
    type: 'iframe',
    mainClass: 'mfp-fade',
    removalDelay: 160,
    preloader: false,

    fixedContentPos: false
  });

  // galeria
   // var $grid = $('.grid-video').isotope({
    // options...
    // itemSelector : '.item-video'
  // });

   
    // init Isotope
    var $grid = $('.grid-video').isotope({
      itemSelector: '.element-video',
      layoutMode: 'fitRows'
    });
    // filter functions
    var filterFns = {
      // show if number is greater than 50
      numberGreaterThan50: function() {
        var number = $(this).find('.number').text();
        return parseInt( number, 10 ) > 50;
      },
      // show if name ends with -ium
      ium: function() {
        var name = $(this).find('.name').text();
        return name.match( /ium$/ );
      }
    };
    // bind filter on select change
    $('.filters-select').on( 'change', function() {
      // get filter value from option value
      var filterValue = this.value;
      // use filterFn if matches value
      filterValue = filterFns[ filterValue ] || filterValue;
      $grid.isotope({ filter: filterValue });
    });
    
  

    // layout Isotope after each image loads
   $grid.imagesLoaded().progress( function() {
     $grid.isotope('layout');
   });
});



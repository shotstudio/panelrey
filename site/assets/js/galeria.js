// @codekit-prepend  "imagesloaded.pkgd.min.js", "isotope.pkgd.min.js",

$(document).ready(function() {

  // galeria
   var $grid = $('.grid2').isotope({
    // options...
    itemSelector : '.item2'
  });
    // layout Isotope after each image loads
   $grid.imagesLoaded().progress( function() {
     $grid.isotope('layout');
   });
});

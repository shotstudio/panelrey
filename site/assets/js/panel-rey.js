// @codekit-prepend  "bootstrap.js", "svgeezy.js", "wow.min.js"


$(document).ready(function() {

     var wow = new WOW(
      {
        boxClass:     'wow',     
        animateClass: 'animated',
        offset:       0,          
        mobile:       false,       
        live:         true,       
        callback:     function(box) {
        }
      }
    );
    wow.init(); 

    // navigation

    function closeNav(){
        $('.sidebar').addClass('escondido');
        $('.supercont').removeClass('moveBody');
        navstatus = 'cerrado';
    }

    var navstatus = 'cerrado';
    $('.nav-btn').click(function(){
        if ( navstatus == 'cerrado' ) {
            $('.sidebar').removeClass('escondido');
            $('.supercont').addClass('moveBody');
            navstatus = 'abierto';
        } else if( navstatus == 'abierto' ){
            closeNav();
        };
        return false;
    });

    $('.side-close').click(function(){
        closeNav();
    });
  


});

var styles = [
	{
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#BAEBFF"
            },
            {
                "lightness": 17
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f5f5f5"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 17
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 29
            },
            {
                "weight": 0.2
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 18
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f5f5f5"
            },
            {
                "lightness": 21
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#dedede"
            },
            {
                "lightness": 21
            }
        ]
    },
    {
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#ffffff"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "saturation": 36
            },
            {
                "color": "#333333"
            },
            {
                "lightness": 40
            }
        ]
    },
    {
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f2f2f2"
            },
            {
                "lightness": 19
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#fefefe"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#fefefe"
            },
            {
                "lightness": 17
            },
            {
                "weight": 1.2
            }
        ]
    }
];

function initialize() {
    var latlng = new google.maps.LatLng(25.668771, -100.326067);
    var myOptions = {
      zoom: 13,
      center: latlng,
      scrollwheel: false,
      mapTypeControlOptions: {
    mapTypeIds: [ 'map_canvas']
    },
      mapTypeId: 'map_canvas',
     
    mapTypeControl: true,
      mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
        position: google.maps.ControlPosition.TOP_RIGHT,
    },
    
    };
    var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
  var styledMapType = new google.maps.StyledMapType(styles, { name: 'map_canvas' });
  map.mapTypes.set('map_canvas', styledMapType)
  
  
  var content_csc = '<div id="content">'+
        '<div id="siteNotice">'+
        '</div>'+
        '<h1 id="firstHeading" class="firstHeading">Panel Rey Monterrey.</h1>'+
        '<div id="bodyContent">'+
        '<p>Serafín Peña 935 Sur, Col. Centro C.P. 64000 El Carmen, Nuevo León</p>' +
        '</div>'+
        '</div>';
    var infowindow_csc = new google.maps.InfoWindow({
        content: content_csc,
    maxWidth: 300
    });
    var marker_csc = new google.maps.Marker({
        position: new google.maps.LatLng (25.668771, -100.326067),
        map: map,
        icon:"assets/images/marker.png",
        title: 'Panel Rey'
    });
    google.maps.event.addListener(marker_csc, 'click', function() {
      infowindow_csc.open(map,marker_csc);
    });
}


$(document).ready(function() {
  initialize();
});
    












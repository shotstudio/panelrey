$(function(){

	// process form 1
	// VALIDACION Y ENVIO
	$( ".form" ).validate({
		errorElement: "div",
		rules: {
			nombre: { required: true },
			email: { required: true, email: true }
		},
		messages: {
			nombre: "Este campo es requerido",
			email: {
				required: "Este campo es requerido",
				email: "Proporciona un email valido, por ejemplo: direccion@dominio.com"
			}
		},
		success: "valid",
		submitHandler: function() {

			var spinner = $(".loading");
			var form = $(".form");
            var datos = form.serialize();

            //console.log(datos);

            var data = new FormData();
			data.append('datos', datos);
			data.append('submit', 'Guardar');

			// ---------------------------------------------------------------
			var jqxhr = $.ajax({
				url: 'process.php',
				type: 'POST',
				data: data,
				contentType: false,
				processData: false,
				cache: false,
				beforeSend: function(){
					//spinner.css("display","inline-block");
				}
			});
			jqxhr.done(function( data ) {

				//spinner.fadeOut("fast");

				if( data.status == 200 ) {

					form[0].reset();
					grecaptcha.reset();

					//MOSTRAR MENSAJE
                    $('.msggracias').slideDown(300,function(){
				        $(this).delay(2000).slideUp(300);
				    });
				} else {
					//MOSTRAR MENSAJE
                    $('.msgnoenviado').text( data.msj ).slideDown(300,function(){
				        $(this).delay(3000).slideUp(300);
				    });
				}
			});
			jqxhr.fail( function() { alert( "error" ); } );
			// ---------------------------------------------------------------
		}

	});

});
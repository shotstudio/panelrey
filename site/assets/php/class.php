<?php

    /*
    * Juan Carlos
    * all_is_design@hotmail.com
    * 30/12/14
    */

    //Limpiar----------------------------------------------------------------
    class Limpiar
    {

        //String
        public function String( $contenido )
        {
            $contenido = htmlspecialchars( trim( $contenido ) );
            $contenido = addslashes( $contenido );
            return $contenido;
        }
        //Numero
        public function Number( $contenido )
        {
            $contenido = ( int ) trim( $contenido );
            return $contenido;
        }
        //Etiquetas TinyMce
        public function TinyMce( $contenido )
        {
            $contenido = str_ireplace("<!DOCTYPE html>","",$contenido);
            $contenido = str_ireplace("<html>","",$contenido);
            $contenido = str_ireplace("<head>","",$contenido);
            $contenido = str_ireplace("</head>","",$contenido);
            $contenido = str_ireplace("<body>","",$contenido);
            $contenido = str_ireplace("</body>","",$contenido);
            $contenido = str_ireplace("</html>","",$contenido);
            $contenido = trim( addslashes( $contenido ) );
            return $contenido;
        }
        //QUITAR ESPACIOS
        public function LimpiarEspacios( $contenido )
        {
            $contenido = str_ireplace(" ","_", $contenido);
            return $contenido;
        }

    }



    //Validar Imagen---------------------------------------------------------
    class ValidarImagen
    {

        private $Atype = array('image/jpg', 'image/jpeg', 'image/pjpeg', 'image/gif', 'image/png','image/x-png');
        private $Aext = array("jpg", "jpeg", "pjpeg", "gif", "png", "x-png", "JPG", "JPEG", "PJPEG", "GIF", "PNG", "X-PNG");
        private $type;
        private $size;
        private $name;
		private $error;
        private $src;
        private $Asize;
        private $extencion;

        public function __construct( $type, $size, $name, $error, $src, $Asize, $extencion )
        {
            $this->type = $type;
            $this->size = $size;
            $this->name = $name;
			$this->error = $error;
            $this->src = $src;
            $this->Asize = $Asize;
            $this->extencion = $extencion;
        }

        public function Validar()
        {
            //Error
            if ( $this->error != 0 )
            {
                switch( $this->error ) {
                    case 1:
                        $msg = $this->name.' excede la directiva upload_max_filesize en php.ini.';
                        break;
                    case 2:
                        $msg = $this->name.' excede la directiva MAX_FILE_SIZE que fue especificada en el formulario HTML.';
                        break;
                    case 4:
                        $msg = 'No se ha seleccionado ningún archivo.';
                        break;
                    case 7:
                        $msg = 'Revisa los permisos de escritura del servidor.';
                        break;
                    default:
                        $msg = 'Hubo un problema al subir '.$this->name.', intenta de nuevo más tarde.';
                }//switch

                echo $msg;
                exit();

            }
            //Error

            //Tamaño del archivo
            if( $this->size > $this->Asize )
            {
                echo 'La imágen excede el tamaño máximo permitido';
                exit();
            }
            //Tipo de archivo
            else if( !in_array($this->type, $this->Atype) || !in_array($this->extencion, $this->Aext) )
            {
                echo 'El tipo de imágen que intentas subir no esta permitido.('.$this->name.')';
                exit();
            }
            //Existencia del archivo
            else if( file_exists($this->src.$this->name) )
            {
                echo 'La imágen que intentas subir ya existe.';
                exit();
            }



        }

    }

    //Validar Imagen---------------------------------------------------------
    class ValidarArchivo
    {
        private $Aext = array("jpg", "jpeg", "pjpeg", "png", "pdf", "pptx", "potx", "ppsx", "docx", "dotx", "PDF", "PPTX", "POTX", "PPSX", "DOCX", "DOTX");
        private $type;
        private $size;
        private $name;
        private $error;
        private $src;
        private $Asize;
        private $extencion;

        public function __construct( $type, $size, $name, $error, $src, $Asize, $extencion )
        {
            $this->type = $type;
            $this->size = $size;
            $this->name = $name;
            $this->error = $error;
            $this->src = $src;
            $this->Asize = $Asize;
            $this->extencion = $extencion;
        }

        public function Validar()
        {
            //Error
            if ( $this->error != 0 )
            {
                switch( $this->error ) {
                    case 1:
                        $msg = $this->name.' excede la directiva upload_max_filesize en php.ini.';
                        break;
                    case 2:
                        $msg = $this->name.' excede la directiva MAX_FILE_SIZE que fue especificada en el formulario HTML.';
                        break;
                    case 4:
                        $msg = 'No se ha seleccionado ningún archivo.';
                        break;
                    case 7:
                        $msg = 'Revisa los permisos de escritura del servidor.';
                        break;
                    default:
                        $msg = 'Hubo un problema al subir '.$this->name.', intenta de nuevo más tarde.';
                }//switch

                echo $msg;
                exit();

            }
            //Error

            //Tamaño del archivo
            if( $this->size > $this->Asize )
            {
                echo json_encode(array('exito'=>false, 'msj'=>'El documento excede el tamaño máximo permitido'));
                exit();
            }
            //Tipo de archivo
            else if( !in_array($this->extencion, $this->Aext) )
            {
                echo json_encode(array('exito'=>false, 'msj'=>'Tipo de documento no admitido'));
                exit();
            }
            //Existencia del archivo
            else if( file_exists($this->src.$this->name) )
            {
                echo json_encode(array('exito'=>false, 'msj'=>'La documento que intentas subir ya existe'));
                exit();
            }

        }

    }

    //Extencion de un archivo---------------------------------------------------
    class ExtencionArchivo
    {
        private $file;

        public function __construct( $file )
        {
            $this->file = $file;
        }
        public function Extencion()
        {
            $extFil = explode('.', $this->file);
            $extFile = end($extFil);
            return $extFile;
        }

    }

    //Renombra Archivos--------------------------------------------------------
    class RenombrarArchivo
    {
        private $file;
        private $ext;

        public function __construct( $file, $ext )
        {
            $this->file = $file;
            $this->ext = $ext;
        }
        public function Renombrar()
        {
            $newName = substr( md5( $this->file ), 0, 7 );
            $numAle = substr( md5( rand( 1, 9999 ) ), 0, 3 );

            $finalName = trim($newName).'_'.$numAle.'.'.$this->ext;
            return $finalName;
        }
    }


    //Mover Archivo al servidor--------------------------------------------------------
    class MoverArchivo
    {
        private $tmp_name;
        private $ruta;
        private $name;

        public function __construct( $tmp_name, $ruta, $name )
        {
            $this->tmp_name = $tmp_name;
            $this->ruta = $ruta;
            $this->name = $name;
        }
        public function Mover()
        {
            //Mover la imagen
            $move = move_uploaded_file( $this->tmp_name, $this->ruta.$this->name );
            if( !$move )
            {
                echo 'Hubo un problema al mover la imagen al servidor.';
                exit();
            }
        }
    }


?>
var gulp = require('gulp'),
    sass = require('gulp-sass'),
    cleancss = require('gulp-clean-css'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    browserSync = require('browser-sync');


// sass config
gulp.task('sass-p', function () {
  gulp.src('./site/assets/css/scss/styles.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./site/assets/css'))
    .pipe(rename({suffix: '.min'}))
    .pipe(cleancss())
    .pipe(gulp.dest('./site/assets/css'))
});


// sass watch on save
gulp.task('watch', function () {
    gulp.watch('./site/assets/css/scss/*.scss', ['sass-p']);
    //gulp.watch('./site/assets/js/*.js', ['concat']);
});


// browser-sync html setup
gulp.task('browser-sync', function() {
    browserSync.init(["./site/assets/css/*.css",
                      "./site/assets/js/*.js",
                      "./site/assets/js/**/*.js",
                      "./site/assets/js/min/*.js",
                      "./site/*.html",
                      "./site/*.php",
                      "./site/assets/sc/**",
                      "./site/admin/**"], {
        /*server: {
            baseDir: "./site/"
        }*/
        proxy: "localhost:8888/panelrey/site",
        //proxy: "localhost/via-cordillera/site",
        open: false
    });
});


gulp.task('default', ['watch', 'browser-sync']);


/*gulp.task('default', ['sass','watch', 'browser-sync'], function () {
    gulp.watch("scss/*.scss", ['sass']);
});*/
